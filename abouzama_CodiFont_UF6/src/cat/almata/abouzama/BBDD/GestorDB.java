package cat.almata.abouzama.BBDD;



import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import cat.almata.abouzama.Excepcions.GestorDBExcception;




public class GestorDB {
	private static Connection connexioActual=null;
	
 

	static {
		try {
			Class.forName(Configuracio.DRIVER_NAME);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private static Connection getConnexio() throws GestorDBExcception {
		if(connexioActual==null) {
			try {
					connexioActual = DriverManager.getConnection(getCadenaConnexio(), Configuracio.USUARI,
								Configuracio.PASSWORD);
			} catch (SQLException e) {
				throw new GestorDBExcception(e.getMessage());
			}
		}
		return connexioActual;
	}
	
	private static String getCadenaConnexio() {
		return Configuracio.PROTOCOL + Configuracio.IP_SERVIDOR + ":" + Configuracio.PORT_BBDD + "/"
				+ Configuracio.BBDD;
	}
	
	private static void closeConnexio() throws GestorDBExcception {
		if(connexioActual!=null) {
			try {
				connexioActual.close();
				connexioActual=null;
			}catch (SQLException e) {
				throw new GestorDBExcception(e.getMessage());
			}
		} 
	}

	public static void modificaDB(String sql) throws GestorDBExcception {
		try (Statement sentencia = getConnexio().createStatement();
		) {
			sentencia.executeUpdate(sql);
		} catch ( SQLException e) {
			//e.printStackTrace();
			throw new GestorDBExcception(e.getMessage());
		}finally {
			closeConnexio();
		}
	}

	public static <T> List<T> consultaDB(String sql, Class<T> classe) throws GestorDBExcception {
		List<T> llistaObjectes = null;
		try (  	Statement sentencia = getConnexio().createStatement();
				ResultSet files = sentencia.executeQuery(sql)) {
			llistaObjectes = resultSetToCollection(files, classe);
		} catch (SQLException e) {
			//e.printStackTrace();
			throw new GestorDBExcception(e.getMessage());
		} catch (InstantiationException e) {
			throw new GestorDBExcception(e.getMessage());
		} catch (IllegalAccessException e) {
			throw new GestorDBExcception(e.getMessage());
		} catch (ClassNotFoundException e) {
			throw new GestorDBExcception(e.getMessage());
		} catch (IllegalArgumentException e) {
			throw new GestorDBExcception(e.getMessage());
		} catch (InvocationTargetException e) {
			throw new GestorDBExcception(e.getMessage());
		}  finally {
			closeConnexio();
		}
		return llistaObjectes;
	}
	
	/*
	 * Per poder utilitzar aquesta classe, concretament "resultSetToCollection", que
	 * utilitza reflexió cal tenir en compte: 
	 * 1) La classe de domini del segon paràmetre ha de tenir el constructor buit. 
	 * 2) No estan controlats tots els tipus basics. Per exemple: si la classe té un 
	 * atribut de tipus char, no funcionaria. En tot cas es pot afegir el control del tipus "char". 
	 * 3) Tots els atributs han de tenir métodes accessors "set..." i "get..." excepte
	 * aquells atributs de tipus Objecte, col·lecció o Array. En aquests casos cal
	 * afegir els mètodes de negoci (addElement,remove,getElemet,...). Per exemple:
	 * en comptes de "set..." posar "add...". A més els atributs d'aquests darrers
	 * tipus s'han de persisitir de forma explícita en una altra consulta. El motiu
	 * és el canvi de paradigma POO->Model Relacional.
	 *
	 * Finalment comentar que es podria millorar el mètode "resultSetToCollection"
	 * per a que acceptés tots els tipus bàsics i potser també els atributs no
	 * simples i tractar-los en crides recursives al mateix mètode
	 * "resultSetToCollection". En tot cas queda pendent de fer....
	 */
	// El suppresswarnings és pel cast "objecte= (T) Class.forName..."
	@SuppressWarnings("unchecked")
	private static <T> List<T> resultSetToCollection(ResultSet files, Class<T> classe)
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException,
			IllegalArgumentException, InvocationTargetException, GestorDBExcception {
		Method[] metodes = null;
		Parameter[] p = null;
		T objecte = null;
		List<T> llistaObjectes = new ArrayList<T>();
		while (files.next()) {
			try {
				objecte = (T) Class.forName(classe.getName()).getDeclaredConstructor().newInstance();
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException | SecurityException
					| ClassNotFoundException e) {
				throw new GestorDBExcception(e.getMessage());
			}
			metodes = objecte.getClass().getDeclaredMethods();
			for (int i = 0; i < metodes.length; i++) {
				if (metodes[i].getName().startsWith("set")) {
					p = metodes[i].getParameters();
					switch (p[0].getType().getName()) {
					case "int":
						metodes[i].invoke(objecte, files.getInt(metodes[i].getName().substring(3)));
						break;
					case "java.lang.Boolean":
						metodes[i].invoke(objecte, files.getBoolean(metodes[i].getName().substring(3)));
						break;
					case "boolean":
						metodes[i].invoke(objecte, files.getBoolean(metodes[i].getName().substring(3)));
						break;
					case "double":
						metodes[i].invoke(objecte, files.getDouble(metodes[i].getName().substring(3)));
						break;
					case "java.lang.String":
						metodes[i].invoke(objecte, files.getString(metodes[i].getName().substring(3).trim()));
						break;
					case "java.time.LocalDate":
						Date d = files.getDate(metodes[i].getName().substring(3));
						LocalDate ld = d.toLocalDate();
						metodes[i].invoke(objecte, ld);
						break;
					}
				}
			}
			llistaObjectes.add(objecte);
		}
		return llistaObjectes;
	}

}
