package cat.almata.abouzama.BBDD;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import cat.almata.abouzama.Excepcions.GestorDBExcception;
import cat.almata.abouzama.domini.Alumne;
import cat.almata.abouzama.domini.Professor;



public class HelperBBDDMem {
	//private static Map<String,Professor> taulaProfessors = new TreeMap<String, Professor>();
	//private static Map<String,Alumne> taulaAlumnes = new TreeMap<String, Alumne>();
	
	// Gestio Profe
	public static void addProfe(Professor professor) throws GestorDBExcception {
		//taulaProfessors.put(professor.getDni(), professor);
		
		GestorDB.modificaDB("INSERT INTO `professors` "
				+ "(`Dni`,"
				+ "`id`,"
				+ "`nomUsuari`,"
				+ "`nom`,"
				+ "`cognoms`,"
				+ "`dataNaixement`,"
				+ "`poblacio`,"
				+ "`CP`,"
				+ "`teoposicions`)"
				+ "VALUES"
				+ "('"+professor.getDni()+"',"
				+ "'"+professor.getId()+"',"
				+ "'"+professor.getNomUsuari()+"',"
				+ "'"+professor.getNom()+"',"
				+ "'"+professor.getCognoms()+"',"
				+ "'"+professor.getDataNaixement()+"', "
				+ "'"+professor.getPoblacio()+"',"
				+ ""+professor.getCP()+","
				+ ""+professor.getTeoposicions()+")");
	}
	
	public static void ModifyProfe(Professor professor) throws GestorDBExcception {
		//taulaProfessors.put(professor.getDni(), professor);
		
		GestorDB.modificaDB("UPDATE `professors`"
				+ "SET"
				+ "`id` =  '"+professor.getId()+"',"
				+ "`nomUsuari` =  '"+professor.getNomUsuari()+"',"
				+ "`nom` =  '"+professor.getNom()+"',"
				+ "`cognoms` =  '"+professor.getCognoms()+"',"
				+ "`dataNaixement` =  '"+professor.getDataNaixement()+"',"
				+ "`poblacio` =  '"+professor.getPoblacio()+"',"
				+ "`CP` =  '"+professor.getCP()+"',"
				+ "`teoposicions` =  "+professor.getTeoposicions()+" "
				+ "WHERE `Dni` =  '"+professor.getDni()+"'");
	}
	

	public static Professor getProfe(String KeyDni) throws GestorDBExcception {
		//System.out.println(taulaProfessors.get(KeyDni));
		String sql = "SELECT * from professors where Dni = '"+KeyDni+"'";
		List<Professor> pr =  GestorDB.consultaDB(sql,Professor.class);
	//	pr.get(0).getteoposicions();
		Professor profe = null;
		if(pr.size()!=0) {
			profe = pr.get(0);
		}
		return profe;
	}
	
	
	public static Map<String,Professor> getAllprofessors() throws GestorDBExcception{
		Map<String,Professor> mapaProfessors = new TreeMap<String, Professor>();
		List<Professor> profe =  GestorDB.consultaDB("SELECT * from professors ", Professor.class);
		for(Professor elem : profe) {
			mapaProfessors.put(elem.getDni(), elem);
		}
		return mapaProfessors;
	}

	public static void removeProfe(String keyDni) throws GestorDBExcception {
		GestorDB.modificaDB("DELETE FROM alumnes where fk_dni_profe = '"+keyDni+"'");
		GestorDB.modificaDB("DELETE FROM professors where dni = '"+keyDni+"'");
		
		 
		//taulaProfessors.remove(keyDni);
	}
	public static void removeAlumne(String keyDni) throws GestorDBExcception {
		GestorDB.modificaDB("DELETE FROM `alumnes`"
				+ "		WHERE Dni = '"+keyDni+"';");
		
	}
	// Gestio Alumne
	public static void addAlumne(Alumne alumne) throws GestorDBExcception {
		GestorDB.modificaDB("INSERT INTO `alumnes`"
				+ "(`Dni`,"
				+ "`id`,`dateNaixement`,"
				+ "`fk_dni_profe`,"
				+ "`nomUsuari`,"
				+ "`descripcio`,"
				+ "`preuMatricula`,"
				+ "`Esqui`,"
				+ "`Natacio`,"
				+ "`Escalada`,"
				+ "`Equitacio`,"
				+ "`Senderisme`)"
				+ " VALUES "
				+ "('"+alumne.getDni()+"',"
				+ "'"+alumne.getId()+"',"
				+ "'"+alumne.getDateNaixement()+"',"
				+ "'"+alumne.getFk_dni_Profe()+"',"
				+ "'"+alumne.getNomUsuari()+"',"
				+ "'"+alumne.getDescripcio()+"',"
				+ ""+alumne.getPreuMatricula()+","
				+ ""+alumne.getEsqui()+","
				+ ""+alumne.getNatacio()+","
				+ ""+alumne.getEscalada()+","
				+ ""+alumne.getEquitacio()+","
				+ ""+alumne.getSenderisme()+")");	
		
	}
	
	
	public static Alumne getAlumne(String KeyDni) throws GestorDBExcception {
		String sql = "SELECT * from alumnes where Dni = '"+KeyDni+"'";
		List<Alumne> alumnes =  GestorDB.consultaDB(sql,Alumne.class);
	//	pr.get(0).getteoposicions();
		return alumnes.get(0);
		
	}
	
	public static Map<String,Alumne> getAllAlmneMap() throws GestorDBExcception{
		Map<String,Alumne> mapaAlumnes = new TreeMap<String, Alumne>();
		List<Alumne> alumnes =  GestorDB.consultaDB("SELECT * from alumnes ", Alumne.class);
		for(Alumne elem : alumnes) {
			mapaAlumnes.put(elem.getDni(), elem);
		}
		return mapaAlumnes;
	}
	public static void ModifyAlumne(Alumne alumne) throws GestorDBExcception {
		//taulaProfessors.put(professor.getDni(), professor);
		
		GestorDB.modificaDB("UPDATE `examen`.`alumnes`"
				+ " SET "
				+ "`id` = '"+alumne.getId()+"',"
				+ "`dateNaixement` = '"+alumne.getDateNaixement()+"',"
				+ "`nomUsuari` = '"+alumne.getNomUsuari()+"', "
				+ "`descripcio` = '"+alumne.getDescripcio()+"', "
				+ "`preuMatricula` = '"+alumne.getPreuMatricula()+"', "
				+ "`Esqui` = "+alumne.getEsqui()+", "
				+ "`Natacio` =  "+alumne.getNatacio()+", "
				+ "`Escalada` =  "+alumne.getEscalada()+", "
				+ "`Equitacio` =  "+alumne.getEquitacio()+", "
				+ "`Senderisme` =  "+alumne.getSenderisme()+" "
				+ "WHERE `Dni` = '"+alumne.getDni()+"'");
	}
	


	
	
	public static ArrayList<Alumne> getAllAlumnedeprofessor(String KeyDni) throws GestorDBExcception {
		ArrayList<Alumne> arryalumnes = new ArrayList<Alumne>();
		List<Alumne> alumnes =  GestorDB.consultaDB("SELECT * from alumnes where fk_dni_profe = '"+KeyDni+"'", Alumne.class);
		for(Alumne elem : alumnes) {
			arryalumnes.add(elem);
		}
		return arryalumnes;
		
	}
	
	
}
