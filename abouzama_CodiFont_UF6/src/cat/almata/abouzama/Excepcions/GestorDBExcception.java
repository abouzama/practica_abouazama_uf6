package cat.almata.abouzama.Excepcions;

public class GestorDBExcception extends Exception {

	private static final long serialVersionUID = 1L;
	
	public GestorDBExcception() {
		super();
	}

	public GestorDBExcception(String message, Throwable cause) {
		super(message, cause);
	}

	public GestorDBExcception(String message) {
		super(message);
	}

	public GestorDBExcception(Throwable cause) {
		super(cause);
	}
	
	
	

}
