package cat.almata.abouzama;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import cat.almata.abouzama.presentacio.ControladorPresentacio;
import cat.almata.abouzama.presentacio.FormulariPerDefecte;

public class Run {

	public static void main(String[] args) throws UnsupportedLookAndFeelException {
		
		 try {
	            UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
	        } catch (Exception ex) {
	            ex.printStackTrace();
	        }

		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				ControladorPresentacio.crearAplicacio();
				ControladorPresentacio.canviFormulari(new FormulariPerDefecte());
			}
		});
	}

}
