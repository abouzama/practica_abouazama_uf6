package cat.almata.abouzama.presentacio;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.ParseException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import com.github.lgooddatepicker.components.DatePicker;

import cat.almata.abouzama.Excepcions.GestorDBExcception;
import cat.almata.abouzama.controladorAplicacio.Gmail;
import cat.almata.abouzama.utils.Util;

public class FormulariAltaProfessor extends JInternalFrame implements Formulari {

	private static final long serialVersionUID = 1L;
	private static final int MAX_DNI = 9;
	private static final int MAX_NOM = 25;
	private static final int MAX_COGNOMS = 45;
	//private static final int MAX_CP = 5;
	// ID
	private JLabel lblid;
	private JTextField txtid;
	// nomusuari
	private JLabel lblnomUsuauri;
	private JTextField txtnomUsuari;
	// nom
	private JLabel lblnom;
	private JTextField txtnom;
	// cognoms
	private JLabel lblcognoms;
	private JTextField txtcognoms;
	// datanaixement
	private JLabel lbldataNaixement;
	private DatePicker dtpdataNaixement;
	// poblacio
	private JLabel lblpoblacio;
	private JTextField txtpoblacio;
	// opocions
	private JLabel lblopos;
	private ButtonGroup grpButtons;
	private JRadioButton rdbSi;
	private JRadioButton rdbNo;
	// cp
	private JLabel lblCP;
	private JTextField txtCP;
	// butons
	private JButton btnAcceptar;
	private JButton btncancelar;
	// DNI
	private JLabel lblDni;
	private JTextField txtDni;
	private JPanel pnlbuttons;

	private Controlador controlador;
	private FormulariAltaProfessor Fma;

	private GridBagLayout layout;

	public FormulariAltaProfessor() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
		setVisible(true);
	}

	@Override
	public void inicialitzacions() {
		getContentPane().setLayout(layout = new GridBagLayout());
		setTitle("Formulari Professor");
		Util.treureBarraTitolInteralFrame(this);
		controlador = new Controlador();
		Fma = this;
	}

	@Override
	public void crearComponents() {
		// id
		lblid = new JLabel("ID:");
		txtid = new JTextField();

		// nomusuari
		lblnomUsuauri = new JLabel("Nom Usuari:");
		txtnomUsuari = new JTextField();

		// nom
		lblnom = new JLabel("Nom:");
		txtnom = new JTextField();
		
		txtnom.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtnom.getText().length() == MAX_NOM) {
					e.consume();
				}
			}
		});

		// cognoms
		lblcognoms = new JLabel("cognoms:");
		txtcognoms = new JTextField();
		
		txtcognoms.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtcognoms.getText().length() == MAX_COGNOMS) {
					e.consume();
				}
			}
		});

		// data Naixement
		lbldataNaixement = new JLabel("Data Naixement:");
		dtpdataNaixement = new DatePicker();

		// poblacio
		lblpoblacio = new JLabel("Poblacio:");
		txtpoblacio = new JTextField();
		// oposicions
		lblopos = new JLabel("Té oposicions");
		rdbNo = new JRadioButton("No");
		rdbSi = new JRadioButton("Si");
		grpButtons = new ButtonGroup();
		grpButtons.add(rdbNo);
		grpButtons.add(rdbSi);

		// CP

		MaskFormatter mascara;
	    try {
	    	
	        mascara = new MaskFormatter("#####"); //  cinco números
	        txtCP = new JFormattedTextField(mascara);
	    	txtCP.setToolTipText("el code Posta hi ha de Tenir 5 numeros");
			/*txtCP.addKeyListener(new KeyAdapter() {
				public void keyTyped(KeyEvent e) {
					if (txtCP.getText().length() == 5) {
						e.consume();
					}
					
				}
			});*/
	    } catch (ParseException e) {
	    	Object[] objOpcions3 = { "Acceptar" };
			JOptionPane.showInternalOptionDialog(Fma.getContentPane(), // Finestra pare
					"EL camp del CP HI HA D SER NUMERIC"+e.getMessage(), // Missatge de la finestra
					"Cerca usuari", // Títol de la finestra
					JOptionPane.NO_OPTION, // Posarem sempre això.
					JOptionPane.INFORMATION_MESSAGE, // Tipus icona
					null, // Si no customitzem la icona. Sinò un ImageIcon
					objOpcions3, // Vector de tipus Object[]
					objOpcions3[0]);// Element per defecte del vector Ob
	        
	    }

		lblCP = new JLabel("CP:");

		// btns
		pnlbuttons = new JPanel();
		btnAcceptar = new JButton("Acceptar");
		btnAcceptar.addActionListener(controlador);
		btnAcceptar.setActionCommand("Acceptar");
		btncancelar = new JButton("Cancelar");
		btncancelar.addActionListener(controlador);
		btncancelar.setActionCommand("Cancelar");
		pnlbuttons.add(btnAcceptar);
		pnlbuttons.add(btncancelar);
		// Dni

		lblDni = new JLabel("DNI:");
		txtDni = new JTextField();
		txtDni.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtDni.getText().length() == MAX_DNI) {
					e.consume();
				}
			}
		});

	}

	@Override
	public void afegirComponents() {
		getContentPane().add(lblid);
		getContentPane().add(txtid);
		getContentPane().add(lblnomUsuauri);
		getContentPane().add(txtnomUsuari);
		getContentPane().add(lblnom);
		getContentPane().add(txtnom);
		getContentPane().add(lblcognoms);
		getContentPane().add(txtcognoms);
		getContentPane().add(lbldataNaixement);
		getContentPane().add(dtpdataNaixement);
		getContentPane().add(lblpoblacio);
		getContentPane().add(txtpoblacio);
		getContentPane().add(lblopos);
		getContentPane().add(rdbNo);
		getContentPane().add(rdbSi);

		getContentPane().add(lblCP);
		getContentPane().add(txtCP);

		getContentPane().add(lblDni);
		getContentPane().add(txtDni);

		getContentPane().add(pnlbuttons);

	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(5, 5, 5, 5);

		// lblDni
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDni, gbc);

		// txtDNi
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtDni, gbc);

		// lblid
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblid, gbc);

		// txtid
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtid, gbc);

		// lbl nom usuari
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblnomUsuauri, gbc);

		// txt Nom Usuari
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtnomUsuari, gbc);

		// lbl Nom
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblnom, gbc);
		// txt Nom
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtnom, gbc);

		// lbl cognoms
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblcognoms, gbc);
		// txt cognoms
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtcognoms, gbc);

		// lbl data naixement
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lbldataNaixement, gbc);

		// txt data naixement
		gbc.gridx = 1;
		gbc.gridy = 5;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(dtpdataNaixement, gbc);

		// lbl poblacio
		gbc.gridx = 0;
		gbc.gridy = 6;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblpoblacio, gbc);

		// txt poblacio
		gbc.gridx = 1;
		gbc.gridy = 6;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtpoblacio, gbc);

		// lbl opos
		gbc.gridx = 0;
		gbc.gridy = 7;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblopos, gbc);

		// rdb Si
		gbc.gridx = 1;
		gbc.gridy = 7;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(rdbSi, gbc);
		// rdb Si
		gbc.gridx = 1;
		gbc.gridy = 8;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(rdbNo, gbc);

		// lbl cp
		gbc.gridx = 0;
		gbc.gridy = 9;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblCP, gbc);

		// txt cp
		gbc.gridx = 1;
		gbc.gridy = 9;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtCP, gbc);

		// pnlButons
		gbc.gridx = 1;
		gbc.gridy = 10;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(pnlbuttons, gbc);

	}

	private class Controlador implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if (obj instanceof JButton) {
				JButton boto = (JButton) obj;
				if (boto.getActionCommand().equals("Acceptar")) {
					System.out.println("Acceptar..");
					Boolean oposicions = null;
					// codi acceptar
					if (rdbNo.isSelected()) {
						oposicions = false;
					} else if (rdbSi.isSelected()) {

						oposicions = true;
					}
					int CP = 00000;
					try {
						CP = Integer.valueOf(txtCP.getText());
					} catch (Exception e2) {

					}

					if (!txtDni.getText().isEmpty()) {

						if (rdbNo.isSelected() || rdbSi.isSelected()) {
							
							try {
								ControladorPresentacio.addProfe(txtDni.getText(), txtid.getText(), txtnomUsuari.getText(),
										txtnom.getText(), txtcognoms.getText(), dtpdataNaixement.getText(),
										txtpoblacio.getText(), CP, oposicions);
								String asunto = "alta professor desde Applicacio java";
								String cuerpo = "has donat d'alta al professor amb DNI:" + txtDni.getText()
										+ "...";
								buidaComponents();
								try {
									Gmail.enviarCorreu(asunto, cuerpo);
								} catch (GestorDBExcception e1) {
									
									Object[] objOpcions3= {"Acceptar"};
									JOptionPane.showInternalOptionDialog(Fma.getContentPane(), //Finestra pare
											"ERROR GMAIL: \n"+e1.getMessage(), //Missatge de la finestra
											"ALTA usuari", //Títol de la finestra
											JOptionPane.NO_OPTION, //Posarem sempre això.
											JOptionPane.WARNING_MESSAGE, //Tipus icona
											null, //Si no customitzem la icona. Sinò un ImageIcon
											objOpcions3, //Vector de tipus Object[]
											objOpcions3[0]);
								}
							} catch (GestorDBExcception e1) {
								Object[] objOpcions3= {"Acceptar"};
								JOptionPane.showInternalOptionDialog(Fma.getContentPane(), //Finestra pare
										"ERROR SQL: \n"+e1.getMessage(), //Missatge de la finestra
										" alumne", //Títol de la finestra
										JOptionPane.NO_OPTION, //Posarem sempre això.
										JOptionPane.WARNING_MESSAGE, //Tipus icona
										null, //Si no customitzem la icona. Sinò un ImageIcon
										objOpcions3, //Vector de tipus Object[]
										objOpcions3[0]);
							}
							
							
							

						} else {
							JOptionPane.showMessageDialog(Fma, "has de seleccionar si te oposicions!!");
						}

					} else {
						JOptionPane.showMessageDialog(Fma, "No pods deixar el Dni buid");
					}

					

				} else if (boto.getActionCommand().equals("Cancelar")) {
					System.out.println("click Cancelar..");
					buidaComponents();
					ControladorPresentacio.canviFormulari(new FormulariPerDefecte());
				}
			}

		}

		private void buidaComponents() {
			txtnom.setText("");
			txtcognoms.setText("");
			txtCP.setText("");
			dtpdataNaixement.setText("");
			grpButtons.clearSelection();
			txtnomUsuari.setText("");
			txtid.setText("");
			txtpoblacio.setText("");
			txtDni.setText("");
			txtDni.grabFocus();
		}

	}

}
