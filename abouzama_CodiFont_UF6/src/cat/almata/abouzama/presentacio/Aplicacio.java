package cat.almata.abouzama.presentacio;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;

import javax.swing.ImageIcon;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;




public class Aplicacio extends JFrame {
	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 500;
	public static final int HEIGHT = 550;
	private JMenu menuGestioProfe;
	private JMenu menuGestioAlumne;
	private JMenuItem opcioAlta;
	private JMenuItem opcioAltaAlumne;
	private JMenuItem opcioModifica;
	private JMenuItem opcioModificaAlumne;
	private Controlador controlador;
	private JMenuItem opcioconsulta;
	private JMenuItem opcioconsultaAlumne;
	private JMenuItem opcioconsultaTots;
	
	
	

	

	public Aplicacio() {
		inicialitzacions();
	}

	private void inicialitzacions() {
		setBounds(0, 0, WIDTH, HEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setTitle("Aplicació");
		
		Container c = this.getContentPane();
		this.setIconImage(new ImageIcon("imatges/user3.png").getImage());
		((JComponent) c).setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		controlador = new Controlador();
		setJMenuBar(crearMenu());
		
	}
	private JMenuBar crearMenu() {
		// instancia la barra de menú
				JMenuBar barraMenu = new JMenuBar();
				// opcio de menu
				menuGestioProfe = new JMenu("Gestió Professor");
				menuGestioAlumne = new JMenu("Gestió Alumne");
				// afegir menu a la barra de menu
				barraMenu.add(menuGestioProfe);
				barraMenu.add(menuGestioAlumne);

				opcioAlta = new JMenuItem("Alta Professor");
				opcioAlta.setMnemonic(KeyEvent.VK_A);
				opcioAlta.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.ALT_MASK));
				menuGestioProfe.add(opcioAlta);
				menuGestioProfe.addSeparator();
				opcioAlta.addActionListener(controlador);
				opcioAlta.setActionCommand("Alta-Professor");
				
				opcioAltaAlumne = new JMenuItem("Alta Alumne");
				opcioAltaAlumne.setMnemonic(KeyEvent.VK_A);
				opcioAltaAlumne.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.ALT_MASK));
				menuGestioAlumne.add(opcioAltaAlumne);
				opcioAltaAlumne.addActionListener(controlador);
				opcioAltaAlumne.setActionCommand("Alta-Alumne");
				menuGestioAlumne.addSeparator();
				
				
				opcioconsulta = new JMenuItem("Consulta Esborra Professor");
				opcioconsulta.setMnemonic(KeyEvent.VK_C);
				opcioconsulta.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.ALT_MASK));
				menuGestioProfe.add(opcioconsulta);
				menuGestioProfe.addSeparator();
				opcioconsulta.addActionListener(controlador);
				opcioconsulta.setActionCommand("Consulta-Professor");
				
				
				opcioconsultaAlumne = new JMenuItem("Consulta Esborra alumne");
				opcioconsultaAlumne.setMnemonic(KeyEvent.VK_C);
				opcioconsultaAlumne.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.ALT_MASK));
				menuGestioAlumne.add(opcioconsultaAlumne);
				menuGestioAlumne.addSeparator();
				opcioconsultaAlumne.addActionListener(controlador);
				opcioconsultaAlumne.setActionCommand("Consulta-alumne");
				
				
				
				opcioconsultaTots = new JMenuItem("Consulta Tots Alumnes profe");
				opcioconsultaTots.setMnemonic(KeyEvent.VK_O);
				opcioconsultaTots.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.ALT_MASK));
				menuGestioProfe.add(opcioconsultaTots);
				menuGestioProfe.addSeparator();
				opcioconsultaTots.addActionListener(controlador);
				opcioconsultaTots.setActionCommand("Consulta-Tots");
				
				opcioModifica = new JMenuItem("Modifica Professor");
				opcioModifica.setMnemonic(KeyEvent.VK_M);
				opcioModifica.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.ALT_MASK));
				menuGestioProfe.add(opcioModifica);
				menuGestioProfe.addSeparator();
				opcioModifica.addActionListener(controlador);
				opcioModifica.setActionCommand("Modifica-Professor");
				
				
				opcioModificaAlumne = new JMenuItem("Modifica Alumne");
				opcioModificaAlumne.setMnemonic(KeyEvent.VK_M);
				opcioModificaAlumne.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.ALT_MASK));
				menuGestioAlumne.add(opcioModificaAlumne);
				menuGestioAlumne.addSeparator();
				opcioModificaAlumne.addActionListener(controlador);
				opcioModificaAlumne.setActionCommand("Modifica-alumne");
		
		return barraMenu;
		
	}

	private class Controlador implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			
			if(obj instanceof JMenuItem && !(obj instanceof JCheckBoxMenuItem)) {
				JMenuItem jmi = (JMenuItem)obj;
				if(jmi.getActionCommand().equals("Alta-Professor")) {
					System.out.println("Alta-Professor");
					ControladorPresentacio.canviFormulari(new FormulariAltaProfessor());
				}else if(jmi.getActionCommand().equals("Consulta-Professor")) {
					ControladorPresentacio.canviFormulari(new FormulariConsultaEsborrarProfessor());
				}else if(jmi.getActionCommand().equals("Modifica-Professor")) {
					ControladorPresentacio.canviFormulari(new FormulariModificaProfessor());
				}else if(jmi.getActionCommand().equals("Alta-Alumne")) {
					ControladorPresentacio.canviFormulari(new FormulariAltaAlumne());
				}else if(jmi.getActionCommand().equals("Consulta-Tots")) {
					ControladorPresentacio.canviFormulari(new FormulariConsultaTots());
				}
				else if(jmi.getActionCommand().equals("Consulta-alumne")) {
					ControladorPresentacio.canviFormulari(new FormulariConsultaEsborrarAlumne());
				}else if(jmi.getActionCommand().equals("Modifica-alumne")) {
					ControladorPresentacio.canviFormulari(new FormulariModificaAlumne());
				}
				
				
			}

		}

	}

	

}
