package cat.almata.abouzama.presentacio;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.util.Map;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.github.lgooddatepicker.components.DatePicker;

import cat.almata.abouzama.Excepcions.GestorDBExcception;
import cat.almata.abouzama.domini.Alumne;

import cat.almata.abouzama.utils.Util;

public class FormulariConsultaEsborrarAlumne extends JInternalFrame implements Formulari {

	private static final long serialVersionUID = 1L;
	private JTextField txtDni;
	private JLabel lblDni;
	private JTextField txtId;
	private JLabel lblId;
	private JLabel lblnomUsuari;
	private JTextField txtnomUsuari;
	private DatePicker dtpDataNaixement;
	private JLabel lbldataNaixement;
	private JTextField txtpreuMatricula;
	private JLabel lblpreuMatricua;

	private JLabel lblActivitats;
	private JPanel pnlActivitats;
	private JCheckBox chbEsqui;
	private JCheckBox chbNatacio;
	private JCheckBox chbEscalada;
	private JCheckBox chbEquitacio;
	private JCheckBox chbSenderisme;

	private JScrollPane jspObservacions;
	private JTextArea txaObservacio;
	private JLabel lblObservacio;

	private JButton btnConsulta;
	private JButton btnCancelar;
	private JPanel pnlBotons;
	private JComboBox<String> comboAlumne;
	private JLabel lblcomboalumne;
	
	private Controlador controlador;
	private GridBagLayout layout;
	private FormulariConsultaEsborrarAlumne Fma;
	private JButton btnEsborrar;
	

	public FormulariConsultaEsborrarAlumne() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
		setVisible(true);
	}

	@Override
	public void inicialitzacions() {
		getContentPane().setLayout(layout = new GridBagLayout());
		setTitle("Formulari Alumne");
		Util.treureBarraTitolInteralFrame(this);
		controlador = new Controlador();
		Fma = this;

	}

	@Override
	public void crearComponents() {
		comboAlumne = new JComboBox<String>();
		omplircombo(comboAlumne);
		comboAlumne.addActionListener(controlador);
		comboAlumne.setActionCommand("Combo");
		lblcomboalumne = new JLabel("Alumne:");
		txtDni = new JTextField();
		lblDni = new JLabel("DNI:");
		txtId = new JTextField();
		lblId = new JLabel("ID:");
		lblnomUsuari = new JLabel("Nom Usuari");
		txtnomUsuari = new JTextField();

		dtpDataNaixement = new DatePicker();
		lbldataNaixement = new JLabel("Date Naixement:");
		txtpreuMatricula = new JTextField();
		lblpreuMatricua = new JLabel("Preu Matricula:");

		lblActivitats = new JLabel("Activitats Escolars:");
		chbEsqui = new JCheckBox("Esqui");
		chbNatacio = new JCheckBox("Natació");
		chbEquitacio = new JCheckBox("Equitacio");
		chbSenderisme = new JCheckBox("Senderisme");
		chbEscalada = new JCheckBox("Escalada");
		pnlActivitats = new JPanel(new GridLayout(2, 3));

		pnlActivitats.add(chbEsqui);
		pnlActivitats.add(chbNatacio);
		pnlActivitats.add(chbEquitacio);
		pnlActivitats.add(chbSenderisme);
		pnlActivitats.add(chbEscalada);

		txaObservacio = new JTextArea();
		lblObservacio = new JLabel("Observacions:");
		txaObservacio.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		jspObservacions = new JScrollPane(txaObservacio);
		jspObservacions.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jspObservacions.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		btnConsulta = new JButton("Consulta-Nova");
		btnConsulta.addActionListener(controlador);
		btnConsulta.setActionCommand("Acceptar");
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(controlador);
		btnCancelar.setActionCommand("Cancelar");
		pnlBotons = new JPanel();
		
		btnEsborrar = new JButton("Borrar");
		btnEsborrar.addActionListener(controlador);
		btnEsborrar.setActionCommand("Borrar");
	
		pnlBotons.add(btnEsborrar);
		pnlBotons.add(btnConsulta);
		pnlBotons.add(btnCancelar);
		
		txtDni.setEnabled(false);
		txtId.setEnabled(false);
		dtpDataNaixement.setEnabled(false);
		chbEquitacio.setEnabled(false);
		chbEscalada.setEnabled(false);
		chbEsqui.setEnabled(false);
		chbNatacio.setEnabled(false);
		chbSenderisme.setEnabled(false);
		txtnomUsuari.setEnabled(false);
		txtpreuMatricula.setEnabled(false);
		txaObservacio.setEnabled(false);
		btnConsulta.setEnabled(false);
		btnEsborrar.setEnabled(false);
		
		
	}
	private void omplircombo(JComboBox<String> comboalmne) {
		Map<String, Alumne> alumnes;
		try {
			alumnes = ControladorPresentacio.getAllAlumneMap();
			comboalmne.addItem("--Selecciona Dni--");
			for (Entry<String, Alumne> Objectemapa : alumnes.entrySet()) {
				Alumne alum = Objectemapa.getValue();
				comboalmne.addItem(alum.getDni());
			}
		} catch (GestorDBExcception e) {
			Object[] objOpcions3= {"Acceptar"};
			JOptionPane.showInternalOptionDialog(Fma.getContentPane(), //Finestra pare
					"ERROR SQL: \n"+e.getMessage(), //Missatge de la finestra
					" alumne", //Títol de la finestra
					JOptionPane.NO_OPTION, //Posarem sempre això.
					JOptionPane.WARNING_MESSAGE, //Tipus icona
					null, //Si no customitzem la icona. Sinò un ImageIcon
					objOpcions3, //Vector de tipus Object[]
					objOpcions3[0]);
		}
		

	}

	@Override
	public void afegirComponents() {
		getContentPane().add(lblcomboalumne);
		getContentPane().add(comboAlumne);
		getContentPane().add(lblDni);
		getContentPane().add(txtDni);
		getContentPane().add(lblId);
		getContentPane().add(txtId);
		getContentPane().add(lblnomUsuari);
		getContentPane().add(txtnomUsuari);
		getContentPane().add(lbldataNaixement);
		getContentPane().add(dtpDataNaixement);
		getContentPane().add(lblpreuMatricua);
		getContentPane().add(txtpreuMatricula);
		getContentPane().add(lblActivitats);
		getContentPane().add(pnlActivitats);
		getContentPane().add(jspObservacions);

		getContentPane().add(pnlBotons);

	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(5, 5, 5, 5);

		// lblDni
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblcomboalumne, gbc);

		// txt Dni
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(comboAlumne, gbc);

		// lblDni
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDni, gbc);

		// txt Dni
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtDni, gbc);

		// lblid
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblId, gbc);

		// txt id
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtId, gbc);

		// lbl nom usuarui
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblnomUsuari, gbc);

		// txt nom usuari
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtnomUsuari, gbc);

		// lbl data naixement
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lbldataNaixement, gbc);

		// txt datanaixement
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(dtpDataNaixement, gbc);

		// lblid
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblpreuMatricua, gbc);

		// txt id
		gbc.gridx = 1;
		gbc.gridy = 5;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtpreuMatricula, gbc);

		// lbl activitats
		gbc.gridx = 0;
		gbc.gridy = 6;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblActivitats, gbc);

		// activitats
		gbc.gridx = 1;
		gbc.gridy = 7;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(pnlActivitats, gbc);

		// lbl observacio
		gbc.gridx = 0;
		gbc.gridy = 8;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblObservacio, gbc);

		gbc.gridx = 0;
		gbc.gridy = 9;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblObservacio, gbc);
		gbc.gridx = 0;
		gbc.gridy = 10;
		gbc.gridheight = 4;
		gbc.gridwidth = 2;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.BOTH;
		layout.setConstraints(jspObservacions, gbc);

		gbc.gridx = 1;
		gbc.gridy = 14;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(pnlBotons, gbc);

	}

	private class Controlador implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if (obj instanceof JButton) {
				JButton boto = (JButton) obj;
				if (boto.getActionCommand().equals("Acceptar")) {
					System.out.println("Acceptar..");
					buidaComponents();
					activarComponents(false);
					comboAlumne.setSelectedIndex(0);
					//btnEsborrar.setEnabled(false);
					

				} else if (boto.getActionCommand().equals("Cancelar")) {
					System.out.println("click Cancelar..");
					buidaComponents();
					ControladorPresentacio.canviFormulari(new FormulariPerDefecte());
				}else if (boto.getActionCommand().equals("Borrar")) {
					System.out.println("click borrar..");
					Object[] objOpcions4= {"Acceptar","Cancel·lar"};
					int n= JOptionPane.showInternalOptionDialog(Fma.getContentPane(), //Finestra pare
					"Segur que Vols donar-lo de baixa? ", //Missatge de la finestra
					"Baixa Alumne", //Títol de la finestra
					JOptionPane.NO_OPTION, //Posarem sempre això.
					JOptionPane.QUESTION_MESSAGE, //Tipus icona
					null, //Si no customitzem la icona. Sinò un ImageIcon
					objOpcions4, //Vector de tipus Object[]
					objOpcions4[0]);//Element per defecte del vector Object[]
					//control del retorn del diàleg (0-primer botó, 1-segon botó, etc)
					if(n==0) {
					
					try {
						ControladorPresentacio.removeAlumne(txtDni.getText());
						comboAlumne.removeItem(comboAlumne.getSelectedItem());
						buidaComponents();
						comboAlumne.setSelectedIndex(0);
					} catch (GestorDBExcception e1) {
						Object[] objOpcions3= {"Acceptar"};
						JOptionPane.showInternalOptionDialog(Fma.getContentPane(), //Finestra pare
								"ERROR SQL: \n"+e1.getMessage(), //Missatge de la finestra
								"ALTA usuari", //Títol de la finestra
								JOptionPane.NO_OPTION, //Posarem sempre això.
								JOptionPane.WARNING_MESSAGE, //Tipus icona
								null, //Si no customitzem la icona. Sinò un ImageIcon
								objOpcions3, //Vector de tipus Object[]
								objOpcions3[0]);
					}
				}
					
				}
			}else if (obj instanceof JComboBox) {
				if (!comboAlumne.getSelectedItem().equals("--Selecciona Dni--")) {
					System.out.println("combo");
					// codi Combo
					comboAlumne.setEnabled(false);
					Alumne alumne;
					try {
						alumne = ControladorPresentacio.getAlumne(String.valueOf(comboAlumne.getSelectedItem()));
						if (alumne != null) {
							txtDni.setText(alumne.getDni());
							txtId.setText(alumne.getId());
							if (alumne.getEsqui()) {
								chbEsqui.setSelected(true);
							} else {
								chbEsqui.setSelected(false);
							}
							if (alumne.getEquitacio()) {
								chbEquitacio.setSelected(true);
							} else {
								chbEquitacio.setSelected(false);
							}
							if (alumne.getEscalada()) {
								chbEscalada.setSelected(true);
							} else {
								chbEscalada.setSelected(false);
							}
							if (alumne.getNatacio()) {
								chbNatacio.setSelected(true);
							} else {
								chbNatacio.setSelected(false);
							}
							if (alumne.getSenderisme()) {
								chbSenderisme.setSelected(true);
							} else {
								chbSenderisme.setSelected(false);
							}
							txtDni.setText(alumne.getDni());
							txtId.setText(alumne.getId());
							dtpDataNaixement.setText(alumne.getDateNaixement());
							txtnomUsuari.setText(alumne.getNomUsuari());
							txtpreuMatricula.setText(String.valueOf(alumne.getPreuMatricula()));
							txaObservacio.setText(alumne.getDescripcio());
							btnConsulta.setEnabled(true);
							activarComponents(false);
						}
					
					} catch (GestorDBExcception e1) {
						Object[] objOpcions3= {"Acceptar"};
						JOptionPane.showInternalOptionDialog(Fma.getContentPane(), //Finestra pare
								"ERROR SQL: \n"+e1.getMessage(), //Missatge de la finestra
								" alumne", //Títol de la finestra
								JOptionPane.NO_OPTION, //Posarem sempre això.
								JOptionPane.WARNING_MESSAGE, //Tipus icona
								null, //Si no customitzem la icona. Sinò un ImageIcon
								objOpcions3, //Vector de tipus Object[]
								objOpcions3[0]);
					}
					

				}

			}

		}
private void activarComponents(Boolean activar) {
			
			if(activar) {
				txtDni.setEnabled(false);
			}else txtDni.setEditable(true);
			
			txtDni.setEnabled(activar);
			txtId.setEnabled(activar);
			dtpDataNaixement.setEnabled(activar);
			chbEquitacio.setEnabled(activar);
			chbEscalada.setEnabled(activar);
			chbEsqui.setEnabled(activar);
			chbNatacio.setEnabled(activar);
			chbSenderisme.setEnabled(activar);
			txtnomUsuari.setEnabled(activar);
			txtpreuMatricula.setEnabled(activar);
			txaObservacio.setEnabled(activar);
			btnConsulta.setEnabled(!activar);
			btnEsborrar.setEnabled(!activar);
			comboAlumne.setEnabled(!activar);
			
		
			
		}

		private void buidaComponents() {
			//comboAlumne.set;
			txtDni.setText("");
			txtId.setText("");
			dtpDataNaixement.setText("");
			chbEquitacio.setSelected(false);
			chbEscalada.setSelected(false);
			chbEsqui.setSelected(false);
			chbNatacio.setSelected(false);
			chbSenderisme.setSelected(false);
			txtnomUsuari.setText("");
			txtpreuMatricula.setText("");
			txaObservacio.setText("");

			// txtDni.setText("");
			txtDni.grabFocus();
		}

	}

}
