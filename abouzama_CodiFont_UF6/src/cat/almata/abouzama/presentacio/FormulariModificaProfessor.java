package cat.almata.abouzama.presentacio;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.ParseException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import com.github.lgooddatepicker.components.DatePicker;

import cat.almata.abouzama.Excepcions.GestorDBExcception;
import cat.almata.abouzama.domini.Professor;

import cat.almata.abouzama.utils.Util;

public class FormulariModificaProfessor extends JInternalFrame implements Formulari {

	private static final long serialVersionUID = 1L;
	private static final int MAX_LONG = 45;
	// ID
	private JLabel lblid;
	private JTextField txtid;
	// nomusuari
	private JLabel lblnomUsuauri;
	private JTextField txtnomUsuari;
	// nom
	private JLabel lblnom;
	private JTextField txtnom;
	// cognoms
	private JLabel lblcognoms;
	private JTextField txtcognoms;
	// datanaixement
	private JLabel lbldataNaixement;
	private DatePicker dtpdataNaixement;
	// poblacio
	private JLabel lblpoblacio;
	private JTextField txtpoblacio;
	// opocions
	private JLabel lblopos;
	private ButtonGroup grpButtons;
	private JRadioButton rdbSi;
	private JRadioButton rdbNo;
	// cp
	private JLabel lblCP;
	private JFormattedTextField txtCP;
	// butons
	private JButton btnAcceptar;
	private JButton btncancelar;
	private JButton btnCercar;
	// DNI
	private JLabel lblDni;
	private JTextField txtDni;
	private JPanel pnlbuttons;

	private Controlador controlador;
	private FormulariModificaProfessor Fma;

	private GridBagLayout layout;

	public FormulariModificaProfessor() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
		setVisible(true);
	}

	@Override
	public void inicialitzacions() {
		getContentPane().setLayout(layout = new GridBagLayout());
		setTitle("Formulari Professor");
		Util.treureBarraTitolInteralFrame(this);
		controlador = new Controlador();
		Fma = this;
	}

	@Override
	public void crearComponents() {
		// id
		lblid = new JLabel("ID:");
		txtid = new JTextField();
		txtid.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtid.getText().length() == 30) {
					e.consume();
				}
			}
		});

		// nomusuari
		lblnomUsuauri = new JLabel("Nom Usuari:");
		txtnomUsuari = new JTextField();
		
		txtnomUsuari.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtnomUsuari.getText().length() == MAX_LONG) {
					e.consume();
				}
			}
		});

		// nom
		lblnom = new JLabel("Nom:");
		txtnom = new JTextField();
		
		txtnom.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtnom.getText().length() == MAX_LONG) {
					e.consume();
				}
			}
		});


		// cognoms
		lblcognoms = new JLabel("cognoms:");
		txtcognoms = new JTextField();
		txtcognoms.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtcognoms.getText().length() == MAX_LONG) {
					e.consume();
				}
			}
		});


		// data Naixement
		lbldataNaixement = new JLabel("Data Naixement:");
		dtpdataNaixement = new DatePicker();
		dtpdataNaixement.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (dtpdataNaixement.getText().length() == MAX_LONG) {
					e.consume();
				}
			}
		});


		// poblacio
		lblpoblacio = new JLabel("Poblacio:");
		txtpoblacio = new JTextField();
		txtpoblacio.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtpoblacio.getText().length() == MAX_LONG) {
					e.consume();
				}
			}
		});

		// oposicions
		lblopos = new JLabel("Té oposicions");
		rdbNo = new JRadioButton("No");
		rdbSi = new JRadioButton("Si");
		grpButtons = new ButtonGroup();
		grpButtons.add(rdbNo);
		grpButtons.add(rdbSi);

		// CP

		
		
		MaskFormatter mascara;
	    try {
	    	
	        mascara = new MaskFormatter("#####"); //  cinco números
	        txtCP = new JFormattedTextField(mascara);
	    	txtCP.setToolTipText("el code Posta hi ha de Tenir 5 numeros");
			/*txtCP.addKeyListener(new KeyAdapter() {
				public void keyTyped(KeyEvent e) {
					if (txtCP.getText().length() == 5) {
						e.consume();
					}
					
				}
			});*/
	    } catch (ParseException e) {
	    	Object[] objOpcions3 = { "Acceptar" };
			JOptionPane.showInternalOptionDialog(Fma.getContentPane(), // Finestra pare
					"EL camp del CP HI HA D SER NUMERIC"+e.getMessage(), // Missatge de la finestra
					"Cerca usuari", // Títol de la finestra
					JOptionPane.NO_OPTION, // Posarem sempre això.
					JOptionPane.INFORMATION_MESSAGE, // Tipus icona
					null, // Si no customitzem la icona. Sinò un ImageIcon
					objOpcions3, // Vector de tipus Object[]
					objOpcions3[0]);// Element per defecte del vector Ob
	        
	    }

		lblCP = new JLabel("CP:");

		// btns
		pnlbuttons = new JPanel();
		btnAcceptar = new JButton("Acceptar");
		btnAcceptar.addActionListener(controlador);
		btnAcceptar.setActionCommand("Acceptar");
		btncancelar = new JButton("Cancelar");
		btncancelar.addActionListener(controlador);
		btncancelar.setActionCommand("Cancelar");
		pnlbuttons.add(btnAcceptar);
		pnlbuttons.add(btncancelar);
		btnCercar = new JButton("Cercar");
		btnCercar.addActionListener(controlador);
		btnCercar.setActionCommand("Cercar");
		// Dni

		lblDni = new JLabel("DNI:");
		txtDni = new JTextField(20);
		
		txtDni.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtDni.getText().length() == 9) {
					e.consume();
				}
			}
		});
		
		txtnom.setEnabled(false);
		txtcognoms.setEnabled(false);

		txtid.setEnabled(false);
		rdbSi.setEnabled(false);
		rdbNo.setEnabled(false);
		txtCP.setEnabled(false);
		btnAcceptar.setEnabled(false);
		txtpoblacio.setEnabled(false);
		txtnomUsuari.setEnabled(false);
		dtpdataNaixement.setEnabled(false);
		

	}

	@Override
	public void afegirComponents() {
		getContentPane().add(lblid);
		getContentPane().add(txtid);
		getContentPane().add(lblnomUsuauri);
		getContentPane().add(txtnomUsuari);
		getContentPane().add(lblnom);
		getContentPane().add(txtnom);
		getContentPane().add(lblcognoms);
		getContentPane().add(txtcognoms);
		getContentPane().add(lbldataNaixement);
		getContentPane().add(dtpdataNaixement);
		getContentPane().add(lblpoblacio);
		getContentPane().add(txtpoblacio);
		getContentPane().add(lblopos);
		getContentPane().add(rdbNo);
		getContentPane().add(rdbSi);

		getContentPane().add(lblCP);
		getContentPane().add(txtCP);

		getContentPane().add(lblDni);
		getContentPane().add(txtDni);

		getContentPane().add(pnlbuttons);
		getContentPane().add(btnCercar);

	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(5, 5, 5, 5);

		// lblDni
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDni, gbc);

		// txtDNi
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 1;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtDni, gbc);

		// btnCercar
		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.fill = GridBagConstraints.NONE;
		layout.setConstraints(btnCercar, gbc);

		// lblid
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblid, gbc);

		// txtid
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 2;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtid, gbc);

		// lbl nom usuari
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblnomUsuauri, gbc);

		// txt Nom Usuari
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.gridheight = 1;
		gbc.gridwidth = 2;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtnomUsuari, gbc);

		// lbl Nom
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblnom, gbc);
		// txt Nom
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.gridheight = 1;
		gbc.gridwidth = 2;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtnom, gbc);

		// lbl cognoms
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblcognoms, gbc);
		// txt cognoms
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.gridheight = 1;
		gbc.gridwidth = 2;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtcognoms, gbc);

		// lbl data naixement
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lbldataNaixement, gbc);

		// txt data naixement
		gbc.gridx = 1;
		gbc.gridy = 5;
		gbc.gridheight = 1;
		gbc.gridwidth = 2;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(dtpdataNaixement, gbc);

		// lbl poblacio
		gbc.gridx = 0;
		gbc.gridy = 6;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblpoblacio, gbc);

		// txt poblacio
		gbc.gridx = 1;
		gbc.gridy = 6;
		gbc.gridheight = 1;
		gbc.gridwidth = 2;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtpoblacio, gbc);

		// lbl opos
		gbc.gridx = 0;
		gbc.gridy = 7;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblopos, gbc);

		// rdb Si
		gbc.gridx = 1;
		gbc.gridy = 7;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(rdbSi, gbc);
		// rdb Si
		gbc.gridx = 1;
		gbc.gridy = 8;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(rdbNo, gbc);

		// lbl cp
		gbc.gridx = 0;
		gbc.gridy = 9;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblCP, gbc);

		// txt cp
		gbc.gridx = 1;
		gbc.gridy = 9;
		gbc.gridheight = 1;
		gbc.gridwidth = 2;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtCP, gbc);

		// pnlButons
		gbc.gridx = 1;
		gbc.gridy = 10;
		gbc.gridheight = 1;
		gbc.gridwidth = 2;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.fill = GridBagConstraints.NONE;
		layout.setConstraints(pnlbuttons, gbc);

	}

	private class Controlador implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if (obj instanceof JButton) {
				JButton boto = (JButton) obj;
				if (boto.getActionCommand().equals("Acceptar")) {
					System.out.println("Acceptar..");
					Boolean oposicions = null;
					// codi acceptar
					if (rdbNo.isSelected()) {
						oposicions = false;
					} else if (rdbSi.isSelected()) {

						oposicions = true;
					}
					int CP = 0;
					try {
						CP = Integer.valueOf(txtCP.getText());
					} catch (Exception e2) {

					}

					if (!txtDni.getText().isEmpty()) {
						try {
							ControladorPresentacio.ModifyProfe(txtDni.getText(), txtid.getText(), txtnomUsuari.getText(),
									txtnom.getText(), txtcognoms.getText(), dtpdataNaixement.getText(),
									txtpoblacio.getText(), CP, oposicions);
						} catch (GestorDBExcception e1) {
							Object[] objOpcions= {"Acceptar"};
							JOptionPane.showInternalOptionDialog(Fma.getContentPane(), //Finestra pare
									"ERROR SQL: \n"+e1.getMessage(), //Missatge de la finestra
									" PROFESSOR", //Títol de la finestra
									JOptionPane.NO_OPTION, //Posarem sempre això.
									JOptionPane.WARNING_MESSAGE, //Tipus icona
									null, //Si no customitzem la icona. Sinò un ImageIcon
									objOpcions, //Vector de tipus Object[]
									objOpcions[0]);
						}
						activarComponents(false);
						buidaComponents();
						
					} else {
						Object[] objOpcions3 = { "Acceptar" };
						JOptionPane.showInternalOptionDialog(Fma.getContentPane(), // Finestra pare
								"EL camp del dni no pot estar en blanc", // Missatge de la finestra
								"Cerca usuari", // Títol de la finestra
								JOptionPane.NO_OPTION, // Posarem sempre això.
								JOptionPane.INFORMATION_MESSAGE, // Tipus icona
								null, // Si no customitzem la icona. Sinò un ImageIcon
								objOpcions3, // Vector de tipus Object[]
								objOpcions3[0]);// Element per defecte del vector Object[]
					}

					
					

				} else if (boto.getActionCommand().equals("Cancelar")) {
					System.out.println("click Cancelar..");
					buidaComponents();
					ControladorPresentacio.canviFormulari(new FormulariPerDefecte());
				} else if (boto.getActionCommand().equals("Cercar")) {
					System.out.println("click Cercar..");
					txtDni.grabFocus();
					// codi Cercar
					Professor profe = null;
					if (!txtDni.getText().isEmpty()) {
						try {
							profe = ControladorPresentacio.getProfe(txtDni.getText());
							if (profe != null) {
								omplirFormulari(profe);
								activarComponents(true);

							} else {
								// NO trobat
								Object[] objOpcions3 = { "Acceptar" };
								JOptionPane.showInternalOptionDialog(Fma.getContentPane(), // Finestra pare
										"Aquest Professor no existeix", // Missatge de la finestra
										"Cerca usuari", // Títol de la finestra
										JOptionPane.NO_OPTION, // Posarem sempre això.
										JOptionPane.INFORMATION_MESSAGE, // Tipus icona
										null, // Si no customitzem la icona. Sinò un ImageIcon
										objOpcions3, // Vector de tipus Object[]
										objOpcions3[0]);// Element per defecte del vector Object[]

							}
						} catch (GestorDBExcception e1) {
							Object[] objOpcions= {"Acceptar"};
							JOptionPane.showInternalOptionDialog(Fma.getContentPane(), //Finestra pare
									"ERROR SQL: \n"+e1.getMessage(), //Missatge de la finestra
									" PROFESSOR", //Títol de la finestra
									JOptionPane.NO_OPTION, //Posarem sempre això.
									JOptionPane.WARNING_MESSAGE, //Tipus icona
									null, //Si no customitzem la icona. Sinò un ImageIcon
									objOpcions, //Vector de tipus Object[]
									objOpcions[0]);
						}
						

					}else {
						Object[] objOpcions3 = { "Acceptar" };
						JOptionPane.showInternalOptionDialog(Fma.getContentPane(), // Finestra pare
								"EL camp del dni no pot estar en blanc", // Missatge de la finestra
								"Cerca usuari", // Títol de la finestra
								JOptionPane.NO_OPTION, // Posarem sempre això.
								JOptionPane.INFORMATION_MESSAGE, // Tipus icona
								null, // Si no customitzem la icona. Sinò un ImageIcon
								objOpcions3, // Vector de tipus Object[]
								objOpcions3[0]);// Element per defecte del vector Object[]
					}
				}

			}

		}

		private void omplirFormulari(Professor profe) {
			if (profe != null) {
				txtDni.setText(profe.getDni());
				txtid.setText(profe.getId());
				if (profe.getTeoposicions()) {
					rdbSi.setSelected(true);
				} else {
					rdbNo.setSelected(true);
				}
				txtCP.setText(String.valueOf(profe.getCP()));
				txtpoblacio.setText(profe.getPoblacio());
				txtcognoms.setText(profe.getCognoms());
				txtnomUsuari.setText(profe.getNomUsuari());
				txtnom.setText(profe.getNom());
				dtpdataNaixement.setText(profe.getDataNaixement());
				btnAcceptar.setEnabled(true);
				btncancelar.setEnabled(true);

			}
		}

		public void activarComponents(Boolean activar) {

			if (activar) {
				txtDni.setEnabled(false);
				btnCercar.setEnabled(false);
				txtid.grabFocus();
			} else {
				txtDni.setEnabled(true);
				btnCercar.setEnabled(true);
				txtDni.grabFocus();
			}

			txtnom.setEnabled(activar);
			txtcognoms.setEnabled(activar);

			txtid.setEnabled(activar);
			rdbSi.setEnabled(activar);
			rdbNo.setEnabled(activar);
			txtCP.setEnabled(activar);
			btnAcceptar.setEnabled(activar);
			txtpoblacio.setEnabled(activar);
			txtnomUsuari.setEnabled(activar);
			dtpdataNaixement.setEnabled(activar);

		}

		private void buidaComponents() {
			txtnom.setText("");
			txtcognoms.setText("");
			txtCP.setText("");
			dtpdataNaixement.setText("");
			grpButtons.clearSelection();
			txtnomUsuari.setText("");
			txtid.setText("");
			txtpoblacio.setText("");
			txtDni.setText("");
			txtDni.grabFocus();
		}

	}

}
