package cat.almata.abouzama.presentacio;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.github.lgooddatepicker.components.DatePicker;

import cat.almata.abouzama.Excepcions.GestorDBExcception;
import cat.almata.abouzama.domini.Alumne;
import cat.almata.abouzama.domini.Professor;
import cat.almata.abouzama.utils.Util;

public class FormulariModificaAlumne extends JInternalFrame implements Formulari {

	private static final long serialVersionUID = 1L;
	private static final int MAX_DNI = 9;
	private static final int MAX_ID = 30;
	
	private static final int MAX_NOMUSUARI = 45;
	private JTextField txtDni;
	private JTextField txtDniProfe;
	private JLabel lblDniProfe;
	private JLabel lblDni;
	private JTextField txtId;
	private JLabel lblId;
	private JLabel lblnomUsuari;
	private JTextField txtnomUsuari;
	private DatePicker dtpDataNaixement;
	private JLabel lbldataNaixement;
	private JTextField txtpreuMatricula;
	private JLabel lblpreuMatricua;

	private JLabel lblActivitats;
	private JPanel pnlActivitats;
	private JCheckBox chbEsqui;
	private JCheckBox chbNatacio;
	private JCheckBox chbEscalada;
	private JCheckBox chbEquitacio;
	private JCheckBox chbSenderisme;

	private JScrollPane jspObservacions;
	private JTextArea txaObservacio;
	private JLabel lblObservacio;

	private JButton btnAcceptar;
	private JButton btnCancelar;
	private JPanel pnlBotons;
	private JComboBox<String> comboAlumne;
	private JLabel lblcomboalumne;
	
	private Controlador controlador;
	private GridBagLayout layout;
	private FormulariModificaAlumne Fma;
	

	public FormulariModificaAlumne() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
		setVisible(true);
	}

	@Override
	public void inicialitzacions() {
		getContentPane().setLayout(layout = new GridBagLayout());
		setTitle("Formulari Alumne");
		Util.treureBarraTitolInteralFrame(this);
		controlador = new Controlador();
		Fma = this;

	}

	@Override
	public void crearComponents() {
		comboAlumne = new JComboBox<String>();
		try {
			omplircombo(comboAlumne);
		} catch (GestorDBExcception e) {
			Object[] objOpcions= {"Acceptar"};
			JOptionPane.showInternalOptionDialog(this.getContentPane(), //Finestra pare
					"ERROR SQL: \n"+e.getMessage(), //Missatge de la finestra
					" PROFESSOR", //Títol de la finestra
					JOptionPane.NO_OPTION, //Posarem sempre això.
					JOptionPane.WARNING_MESSAGE, //Tipus icona
					null, //Si no customitzem la icona. Sinò un ImageIcon
					objOpcions, //Vector de tipus Object[]
					objOpcions[0]);
		}
		comboAlumne.addActionListener(controlador);
		comboAlumne.setActionCommand("Combo");
		lblcomboalumne = new JLabel("Alumne:");
		txtDni = new JTextField();
		
		txtDni.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtDni.getText().length() == MAX_DNI) {
					e.consume();
				}
			}
		});
		lblDni = new JLabel("DNI:");
		txtDniProfe = new JTextField();
		lblDniProfe = new JLabel("DNI Profe:");
		
		txtDniProfe.addKeyListener(new KeyAdapter() {
		

			public void keyTyped(KeyEvent e) {
				if (txtDniProfe.getText().length() == MAX_DNI) {
					e.consume();
				}
			}
		});
		txtId = new JTextField();
		lblId = new JLabel("ID:");
		txtId.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtId.getText().length() == MAX_ID) {
					e.consume();
				}
			}
		});
		lblnomUsuari = new JLabel("Nom Usuari");
		txtnomUsuari = new JTextField();
		
		txtnomUsuari.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txtnomUsuari.getText().length() == MAX_NOMUSUARI) {
					e.consume();
				}
			}
		});

		dtpDataNaixement = new DatePicker();
		lbldataNaixement = new JLabel("Date Naixement:");
		dtpDataNaixement.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (dtpDataNaixement.getText().length() == MAX_NOMUSUARI) {
					e.consume();
				}
			}
		});
		txtpreuMatricula = new JTextField();
		lblpreuMatricua = new JLabel("Preu Matricula:");

		lblActivitats = new JLabel("Activitats Escolars:");
		chbEsqui = new JCheckBox("Esqui");
		chbNatacio = new JCheckBox("Natació");
		chbEquitacio = new JCheckBox("Equitacio");
		chbSenderisme = new JCheckBox("Senderisme");
		chbEscalada = new JCheckBox("Escalada");
		pnlActivitats = new JPanel(new GridLayout(2, 3));

		pnlActivitats.add(chbEsqui);
		pnlActivitats.add(chbNatacio);
		pnlActivitats.add(chbEquitacio);
		pnlActivitats.add(chbSenderisme);
		pnlActivitats.add(chbEscalada);

		txaObservacio = new JTextArea();
		lblObservacio = new JLabel("Observacions:");
		txaObservacio.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		txaObservacio.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				if (txaObservacio.getText().length() == 255) {
					e.consume();
				}
			}
		});
		jspObservacions = new JScrollPane(txaObservacio);
		jspObservacions.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jspObservacions.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		btnAcceptar = new JButton("Acceptar");
		btnAcceptar.addActionListener(controlador);
		btnAcceptar.setActionCommand("Acceptar");
		btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(controlador);
		btnCancelar.setActionCommand("Cancelar");
		pnlBotons = new JPanel();
		
		pnlBotons.add(btnAcceptar);
		pnlBotons.add(btnCancelar);
		
		txtDni.setEnabled(false);
		txtId.setEnabled(false);
		txtDniProfe.setEnabled(false);
		dtpDataNaixement.setEnabled(false);
		chbEquitacio.setEnabled(false);
		chbEscalada.setEnabled(false);
		chbEsqui.setEnabled(false);
		chbNatacio.setEnabled(false);
		chbSenderisme.setEnabled(false);
		txtnomUsuari.setEnabled(false);
		txtpreuMatricula.setEnabled(false);
		txaObservacio.setEnabled(false);
		btnAcceptar.setEnabled(false);
		
		
		
	}
	private void omplircombo(JComboBox<String> comboalmne) throws GestorDBExcception {
		Map<String, Alumne> alumnes = ControladorPresentacio.getAllAlumneMap();
		comboalmne.addItem("--Selecciona Dni--");
		for (Entry<String, Alumne> Objectemapa : alumnes.entrySet()) {
			Alumne alum = Objectemapa.getValue();
			comboalmne.addItem(alum.getDni());
		}

	}

	@Override
	public void afegirComponents() {
		getContentPane().add(lblcomboalumne);
		getContentPane().add(comboAlumne);
		getContentPane().add(lblDni);
		getContentPane().add(txtDni);
		getContentPane().add(lblDniProfe);
		getContentPane().add(txtDniProfe);
		getContentPane().add(lblId);
		getContentPane().add(txtId);
		getContentPane().add(lblnomUsuari);
		getContentPane().add(txtnomUsuari);
		getContentPane().add(lbldataNaixement);
		getContentPane().add(dtpDataNaixement);
		getContentPane().add(lblpreuMatricua);
		getContentPane().add(txtpreuMatricula);
		getContentPane().add(lblActivitats);
		getContentPane().add(pnlActivitats);
		getContentPane().add(jspObservacions);

		getContentPane().add(pnlBotons);

	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(5, 5, 5, 5);

		// lblDni
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblcomboalumne, gbc);

		// txt Dni
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(comboAlumne, gbc);

		// lblDni
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDni, gbc);

		// txt Dni
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtDni, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDniProfe, gbc);

		// txt Dni
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtDniProfe, gbc);

		// lblid
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblId, gbc);

		// txt id
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtId, gbc);

		// lbl nom usuarui
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblnomUsuari, gbc);

		// txt nom usuari
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtnomUsuari, gbc);

		// lbl data naixement
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lbldataNaixement, gbc);

		// txt datanaixement
		gbc.gridx = 1;
		gbc.gridy = 5;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(dtpDataNaixement, gbc);

		// lblid
		gbc.gridx = 0;
		gbc.gridy = 6;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblpreuMatricua, gbc);

		// txt id
		gbc.gridx = 1;
		gbc.gridy = 6;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtpreuMatricula, gbc);

		// lbl activitats
		gbc.gridx = 0;
		gbc.gridy = 7;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblActivitats, gbc);

		// activitats
		gbc.gridx = 1;
		gbc.gridy = 8;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(pnlActivitats, gbc);

		// lbl observacio
		gbc.gridx = 0;
		gbc.gridy = 9;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblObservacio, gbc);

		gbc.gridx = 0;
		gbc.gridy = 10;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblObservacio, gbc);
		gbc.gridx = 0;
		gbc.gridy = 11;
		gbc.gridheight = 4;
		gbc.gridwidth = 2;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.BOTH;
		layout.setConstraints(jspObservacions, gbc);

		gbc.gridx = 1;
		gbc.gridy = 15;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(pnlBotons, gbc);

	}

	private class Controlador implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if (obj instanceof JButton) {
				JButton boto = (JButton) obj;
				if (boto.getActionCommand().equals("Acceptar")) {
					System.out.println("Acceptar..");
					
					System.out.println("Acceptar..");
					Boolean Esqui = false;
					Boolean Natacio = false;
					Boolean Escalada = false;
					Boolean Equitacio = false;
					Boolean Sendirisme = false;
					// codi acceptar
					if (chbEsqui.isSelected()) {
						Esqui = true;
					}
					if (chbNatacio.isSelected()) {
						Natacio = true;
					}
					if (chbEquitacio.isSelected()) {
						Equitacio = true;
					}
					if (chbSenderisme.isSelected()) {
						Sendirisme = true;
					}
					if (chbEscalada.isSelected()) {
						Escalada = true;
					}

					double preu = 0;
					try {
						preu = Double.valueOf(txtpreuMatricula.getText());
					} catch (Exception e2) {

					}
					Professor profe;
					try {
						profe = ControladorPresentacio.getProfe(txtDniProfe.getText());
						if (profe != null) {
							if (!txtDni.getText().equals("")) {
								ControladorPresentacio.ModifyAlumne(txtId.getText(), txtDni.getText(), txtnomUsuari.getText(),
										txaObservacio.getText(), preu, Esqui, Natacio, Escalada, Equitacio, Sendirisme,
										txtDniProfe.getText(),dtpDataNaixement.getText());
								buidaComponents();
								activarComponents(false);
								comboAlumne.setSelectedIndex(0);
							} else {
								JOptionPane.showMessageDialog(Fma, "No pods deixar el Dni del Profe buid");
							}
							
							}
					} catch (GestorDBExcception e1) {
						Object[] objOpcions= {"Acceptar"};
						JOptionPane.showInternalOptionDialog(Fma.getContentPane(), //Finestra pare
								"ERROR SQL: \n"+e1.getMessage(), //Missatge de la finestra
								" PROFESSOR", //Títol de la finestra
								JOptionPane.NO_OPTION, //Posarem sempre això.
								JOptionPane.WARNING_MESSAGE, //Tipus icona
								null, //Si no customitzem la icona. Sinò un ImageIcon
								objOpcions, //Vector de tipus Object[]
								objOpcions[0]);
					}
					
						

				} else if (boto.getActionCommand().equals("Cancelar")) {
					System.out.println("click Cancelar..");
					buidaComponents();
					ControladorPresentacio.canviFormulari(new FormulariPerDefecte());
				}
			}else if (obj instanceof JComboBox) {
				if (!comboAlumne.getSelectedItem().equals("--Selecciona Dni--")) {
					System.out.println("combo");
					// codi Combo
					comboAlumne.setEnabled(false);
					Alumne alumne;
					try {
						alumne = ControladorPresentacio.getAlumne(String.valueOf(comboAlumne.getSelectedItem()));
						if (alumne != null) {
							txtDni.setText(alumne.getDni());
							txtId.setText(alumne.getId());
							if (alumne.getEsqui()) {
								chbEsqui.setSelected(true);
							} else {
								chbEsqui.setSelected(false);
							}
							if (alumne.getEquitacio()) {
								chbEquitacio.setSelected(true);
							} else {
								chbEquitacio.setSelected(false);
							}
							if (alumne.getEscalada()) {
								chbEscalada.setSelected(true);
							} else {
								chbEscalada.setSelected(false);
							}
							if (alumne.getNatacio()) {
								chbNatacio.setSelected(true);
							} else {
								chbNatacio.setSelected(false);
							}
							if (alumne.getSenderisme()) {
								chbSenderisme.setSelected(true);
							} else {
								chbSenderisme.setSelected(false);
							}
							txtDniProfe.setText(alumne.getFk_dni_Profe());
							txtDni.setText(alumne.getDni());
							txtId.setText(alumne.getId());
							dtpDataNaixement.setText(alumne.getDateNaixement());
							txtnomUsuari.setText(alumne.getNomUsuari());
							txtpreuMatricula.setText(String.valueOf(alumne.getPreuMatricula()));
							txaObservacio.setText(alumne.getDescripcio());
							//btnAcceptar.setEnabled(true);
							activarComponents(true);
						}
					} catch (GestorDBExcception e1) {
						Object[] objOpcions= {"Acceptar"};
						JOptionPane.showInternalOptionDialog(Fma.getContentPane(), //Finestra pare
								"ERROR SQL: \n"+e1.getMessage(), //Missatge de la finestra
								" PROFESSOR", //Títol de la finestra
								JOptionPane.NO_OPTION, //Posarem sempre això.
								JOptionPane.WARNING_MESSAGE, //Tipus icona
								null, //Si no customitzem la icona. Sinò un ImageIcon
								objOpcions, //Vector de tipus Object[]
								objOpcions[0]);
					}
					

				}

			}

		}
private void activarComponents(Boolean activar) {
			
			
			
			txtDni.setEnabled(!activar);
			txtId.setEnabled(activar);
			dtpDataNaixement.setEnabled(activar);
			chbEquitacio.setEnabled(activar);
			chbEscalada.setEnabled(activar);
			chbEsqui.setEnabled(activar);
			chbNatacio.setEnabled(activar);
			chbSenderisme.setEnabled(activar);
			txtnomUsuari.setEnabled(activar);
			txtpreuMatricula.setEnabled(activar);
			txaObservacio.setEnabled(activar);
			btnAcceptar.setEnabled(activar);
			
			comboAlumne.setEnabled(!activar);
			
		
			
		}

		private void buidaComponents() {
			//comboAlumne.set;
			txtDni.setText("");
			txtDniProfe.setText("");
			txtId.setText("");
			dtpDataNaixement.setText("");
			chbEquitacio.setSelected(false);
			chbEscalada.setSelected(false);
			chbEsqui.setSelected(false);
			chbNatacio.setSelected(false);
			chbSenderisme.setSelected(false);
			txtnomUsuari.setText("");
			txtpreuMatricula.setText("");
			txaObservacio.setText("");

			// txtDni.setText("");
			txtId.grabFocus();
		}

	}

}
