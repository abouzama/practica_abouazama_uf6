package cat.almata.abouzama.presentacio;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JInternalFrame;

import cat.almata.abouzama.BBDD.HelperBBDDMem;
import cat.almata.abouzama.Excepcions.GestorDBExcception;
import cat.almata.abouzama.domini.Alumne;
import cat.almata.abouzama.domini.Professor;




public class ControladorPresentacio {

	private static Aplicacio aplicacio = null;
	private static JInternalFrame formulariActual = null;
	
	
	public static void crearAplicacio() {
		if(aplicacio == null) {
			aplicacio = new Aplicacio();
		}
	}
	public static void canviFormulari(JInternalFrame nouformulari) {
		if(formulariActual!=null) {
			formulariActual.dispose();
		}
			aplicacio.getContentPane().add(nouformulari,BorderLayout.CENTER);
			aplicacio.setVisible(true);
			formulariActual = nouformulari;

	}
	//metodes de negoci 
		public static void addProfe(String Dni,String id, String nomUsuari, String nom, String cognoms, String dataNaixement, String poblacio,
				int cP, Boolean teoposicions) throws GestorDBExcception {
			HelperBBDDMem.addProfe(new Professor(Dni,id, nomUsuari, nom, cognoms, dataNaixement, poblacio, cP, teoposicions));
		}
		
		public static void addAlumne(String id,String dni, String nomUsuari,String descripcio,double preuMatricula,boolean Esqui,boolean Natacio,
				boolean Escalada,boolean Equitacio,boolean Senderisme,String dniprofe,String datenaixement) throws GestorDBExcception {
			HelperBBDDMem.addAlumne(new Alumne(id,dni,nomUsuari,descripcio,preuMatricula,Esqui,Natacio,Escalada,Equitacio,Senderisme,dniprofe,datenaixement));
		}
		public static Professor getProfe(String KeyDni) throws GestorDBExcception {
			return HelperBBDDMem.getProfe(KeyDni);
		}
		public static Alumne getAlumne(String KeyDni) throws GestorDBExcception {
			return HelperBBDDMem.getAlumne(KeyDni);
		}
		public static Map<String,Professor> getAllprofessors() throws GestorDBExcception{
			return HelperBBDDMem.getAllprofessors();
		}
		public static Map<String,Alumne> getAllAlumneMap() throws GestorDBExcception{
			return HelperBBDDMem.getAllAlmneMap();
		}
		public static void removeProfe(String KeyDni) throws GestorDBExcception {
		 HelperBBDDMem.removeProfe(KeyDni);
	}
		public static void removeAlumne(String KeyDni) throws GestorDBExcception {
			 HelperBBDDMem.removeAlumne(KeyDni);
		}
		public static void ModifyAlumne(String id,String dni, String nomUsuari,String descripcio,double preuMatricula,boolean Esqui,boolean Natacio,
				boolean Escalada,boolean Equitacio,boolean Senderisme,String dniprofe,String datenaixement) throws GestorDBExcception {
			HelperBBDDMem.ModifyAlumne(new Alumne(id,dni,nomUsuari,descripcio,preuMatricula,Esqui,Natacio,Escalada,Equitacio,Senderisme,dniprofe,datenaixement));
		}
		public static void ModifyProfe(String Dni,String id, String nomUsuari, String nom, String cognoms, String dataNaixement, String poblacio,
				int cP, Boolean teoposicions) throws GestorDBExcception {
			HelperBBDDMem.ModifyProfe(new Professor(Dni,id, nomUsuari, nom, cognoms, dataNaixement, poblacio, cP, teoposicions));
		}
		public static ArrayList<Alumne> getAllAlumne(String KeyDni) throws GestorDBExcception {
			//System.out.println(taulaProfessors.get(KeyDni));	
			return HelperBBDDMem.getAllAlumnedeprofessor(KeyDni);
		}
			
}
