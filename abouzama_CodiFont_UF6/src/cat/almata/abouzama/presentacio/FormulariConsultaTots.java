package cat.almata.abouzama.presentacio;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import cat.almata.abouzama.Excepcions.GestorDBExcception;
import cat.almata.abouzama.domini.Alumne;
import cat.almata.abouzama.domini.Professor;
import cat.almata.abouzama.utils.Util;






public class FormulariConsultaTots extends JInternalFrame implements Formulari {

	private static final long serialVersionUID = 1L;
	
	private GridBagLayout layout;
	private Controlador controlador;
	private JButton btnSortir;
	private JTable taula;
	private DefaultTableModel model;
	private JScrollPane jsptaula;
	private JComboBox<String> comboprofessors;
	private JInternalFrame FCT;
	
	
	public FormulariConsultaTots(){
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
	}

	@Override
	public void inicialitzacions() {
		getContentPane().setLayout(layout = new GridBagLayout());
		setTitle("Formulari Consulta usuaris");
		controlador = new Controlador();
		setVisible(true);
		 Util.treureBarraTitolInteralFrame(this);
		 FCT = this;
	}

	@Override
	public void crearComponents() {
		btnSortir = new JButton("Sortir");
		btnSortir.addActionListener(controlador);
		btnSortir.setActionCommand("Sortir");
		//taula 
		model = new DefaultTableModel();
		model.addColumn("DNI");
		model.addColumn("Nom Usuari");
		model.addColumn("Id");
		
		taula = new JTable(model);
		jsptaula = new JScrollPane(taula);
		 comboprofessors = new JComboBox<String>() ;
	
		try {
			omplircombo(comboprofessors);
		} catch (GestorDBExcception e) {
			Object[] objOpcions= {"Acceptar"};
			JOptionPane.showInternalOptionDialog(this.getContentPane(), //Finestra pare
					"ERROR SQL: \n"+e.getMessage(), //Missatge de la finestra
					" PROFESSOR", //Títol de la finestra
					JOptionPane.NO_OPTION, //Posarem sempre això.
					JOptionPane.WARNING_MESSAGE, //Tipus icona
					null, //Si no customitzem la icona. Sinò un ImageIcon
					objOpcions, //Vector de tipus Object[]
					objOpcions[0]);
		}
		comboprofessors.addActionListener(controlador);
		comboprofessors.setActionCommand("Combo");
		
		
		
		
	}
	private void omplircombo(JComboBox<String> comboprofessors2) throws GestorDBExcception {
		Map<String, Professor> professors = ControladorPresentacio.getAllprofessors();
		comboprofessors2.addItem("--Selecciona Dni--");
		for (Entry<String, Professor> Objectemapa : professors.entrySet()) {
			Professor profe = Objectemapa.getValue();
			comboprofessors2.addItem(profe.getDni());
		}

	}

	private void omplirTaula(JComboBox<String> comboprofessors2) throws GestorDBExcception {
	ArrayList<Alumne> alumnes = ControladorPresentacio.getAllAlumne(String.valueOf(comboprofessors2.getSelectedItem()));
	
	for(Alumne alum : alumnes) {		
			Object[] objFila = new Object[3];//tamany segons columnes de la taula
			objFila[0] = alum.getDni();
			objFila[1] = alum.getNomUsuari();
			objFila[2] = alum.getId();
			
			model.addRow(objFila);
		}
	
		
	}

	@Override
	public void afegirComponents() {
		getContentPane().add(btnSortir);
		getContentPane().add(jsptaula);
		getContentPane().add(comboprofessors);
	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();
		// per Deixar espai entre components i Aquest es el Mateix per tots els
		// Componenets
		gbc.insets = new Insets(5, 5, 5, 5);
		
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(comboprofessors, gbc);
		// Posiciona lbldades
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridheight = 4;
		gbc.gridwidth = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		//gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.BOTH;
		layout.setConstraints(jsptaula, gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.NONE;
		layout.setConstraints(btnSortir, gbc);
	}
	private class Controlador implements ActionListener{

		

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if (obj instanceof JButton) {
				JButton boto = (JButton) obj;
			 if (boto.getActionCommand().equals("Sortir")) {
				System.out.println("click Cancelar..");
				// codi CAncelar
				ControladorPresentacio.canviFormulari(new FormulariPerDefecte());
			}
			}
			else {
				int numDatos = model.getRowCount();
				for (int i = 0; i < numDatos; i++) {
				 model.removeRow(0);
				}
				try {
					omplirTaula(comboprofessors);
				} catch (GestorDBExcception e1) {
					Object[] objOpcions= {"Acceptar"};
					JOptionPane.showInternalOptionDialog(FCT.getContentPane(), //Finestra pare
							"ERROR SQL: \n"+e1.getMessage(), //Missatge de la finestra
							" PROFESSOR", //Títol de la finestra
							JOptionPane.NO_OPTION, //Posarem sempre això.
							JOptionPane.WARNING_MESSAGE, //Tipus icona
							null, //Si no customitzem la icona. Sinò un ImageIcon
							objOpcions, //Vector de tipus Object[]
							objOpcions[0]);
				}
			}
			
		}
		
	}

}
