package cat.almata.abouzama.presentacio;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Map.Entry;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import cat.almata.abouzama.Excepcions.GestorDBExcception;
import cat.almata.abouzama.controladorAplicacio.Gmail;
import cat.almata.abouzama.domini.Professor;
import cat.almata.abouzama.utils.Util;

public class FormulariConsultaEsborrarProfessor extends JInternalFrame implements Formulari {

	private static final long serialVersionUID = 1L;
	// ID
	private JLabel lblid;
	private JTextField txtid;
	// nomusuari
	private JLabel lblnomUsuauri;
	private JTextField txtnomUsuari;
	// nom
	private JLabel lblnom;
	private JTextField txtnom;
	// cognoms
	private JLabel lblcognoms;
	private JTextField txtcognoms;
	// datanaixement
	private JLabel lbldataNaixement;
	private JTextField txtdataNaixement;
	// poblacio
	private JLabel lblpoblacio;
	private JTextField txtpoblacio;
	// opocions
	private JLabel lblopos;
	private ButtonGroup grpButtons;
	private JRadioButton rdbSi;
	private JRadioButton rdbNo;
	// cp
	private JLabel lblCP;
	private JTextField txtCP;
	// butons
	private JButton btnConsultanova;
	private JButton btnSortir;
	private JButton btnEsborrar;
	// DNI
	private JLabel lblDni;
	private JTextField txtDni;
	private JPanel pnlbuttons;
	// COMBO BOX
	private JComboBox<String> comboprofessors;
	private JLabel lblcombo;

	private Controlador controlador;
	private FormulariConsultaEsborrarProfessor Formulariconsultaesborra;

	private GridBagLayout layout;

	public FormulariConsultaEsborrarProfessor() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
		setVisible(true);
	}

	@Override
	public void inicialitzacions() {
		getContentPane().setLayout(layout = new GridBagLayout());
		setTitle("Formulari Consulta Esborrar Professor");
		Util.treureBarraTitolInteralFrame(this);
		controlador = new Controlador();
		Formulariconsultaesborra = this;
	}

	@Override
	public void crearComponents() {
		// id
		lblid = new JLabel("ID:");
		txtid = new JTextField();
		txtid.setEnabled(false);

		// nomusuari
		lblnomUsuauri = new JLabel("Nom Usuari:");
		txtnomUsuari = new JTextField();
		txtnomUsuari.setEnabled(false);

		// nom
		lblnom = new JLabel("Nom:");
		txtnom = new JTextField();
		txtnom.setEnabled(false);

		// cognoms
		lblcognoms = new JLabel("cognoms:");
		txtcognoms = new JTextField(30);
		txtcognoms.setEnabled(false);

		// data Naixement
		lbldataNaixement = new JLabel("Data Naixement:");
		txtdataNaixement = new JTextField();
		txtdataNaixement.setEnabled(false);

		// poblacio
		lblpoblacio = new JLabel("Poblacio:");
		txtpoblacio = new JTextField();
		txtpoblacio.setEnabled(false);
		// oposicions
		lblopos = new JLabel("Té oposicions");
		rdbNo = new JRadioButton("No");
		rdbSi = new JRadioButton("Si");
		grpButtons = new ButtonGroup();
		grpButtons.add(rdbNo);
		grpButtons.add(rdbSi);
		rdbNo.setEnabled(false);
		rdbSi.setEnabled(false);

		// CP

		txtCP = new JFormattedTextField();
		txtCP.setToolTipText("el code Postal hi ha de Tenir 5 numeros");
		txtCP.setEnabled(false);
		lblCP = new JLabel("CP:");

		// btns
		pnlbuttons = new JPanel();
		btnConsultanova = new JButton("Consulta-Nova");
		btnConsultanova.addActionListener(controlador);
		btnConsultanova.setActionCommand("Acceptar");
		btnConsultanova.setEnabled(false);
		btnSortir = new JButton("Sortir");
		btnSortir.addActionListener(controlador);
		btnSortir.setActionCommand("Sortir");
		btnEsborrar = new JButton("Borrar");
		btnEsborrar.addActionListener(controlador);
		btnEsborrar.setActionCommand("Borrar");
		btnEsborrar.setEnabled(false);
		pnlbuttons.add(btnEsborrar);
		pnlbuttons.add(btnConsultanova);
		
		pnlbuttons.add(btnSortir);
		// Dni

		lblDni = new JLabel("DNI:");
		txtDni = new JTextField();
		txtDni.setEnabled(false);
		// combo box
		comboprofessors = new JComboBox<String>();
		omplircombo(comboprofessors);
		comboprofessors.addActionListener(controlador);
		comboprofessors.setActionCommand("Combo");
		lblcombo = new JLabel("profe:");

	}

	private void omplircombo(JComboBox<String> comboprofessors2) {
		Map<String, Professor> professors;
		try {
			professors = ControladorPresentacio.getAllprofessors();
			comboprofessors2.addItem("--Selecciona Dni--");
			for (Entry<String, Professor> Objectemapa : professors.entrySet()) {
				Professor profe = Objectemapa.getValue();
				comboprofessors2.addItem(profe.getDni());
			}
		} catch (GestorDBExcception e) {
			Object[] objOpcions3= {"Acceptar"};
			JOptionPane.showInternalOptionDialog(this.getContentPane(), //Finestra pare
					"ERROR SQL: \n"+e.getMessage(), //Missatge de la finestra
					" alumne", //Títol de la finestra
					JOptionPane.NO_OPTION, //Posarem sempre això.
					JOptionPane.WARNING_MESSAGE, //Tipus icona
					null, //Si no customitzem la icona. Sinò un ImageIcon
					objOpcions3, //Vector de tipus Object[]
					objOpcions3[0]);
		}
		

	}

	@Override
	public void afegirComponents() {
		getContentPane().add(lblid);
		getContentPane().add(txtid);
		getContentPane().add(lblnomUsuauri);
		getContentPane().add(txtnomUsuari);
		getContentPane().add(lblnom);
		getContentPane().add(txtnom);
		getContentPane().add(lblcognoms);
		getContentPane().add(txtcognoms);
		getContentPane().add(lbldataNaixement);
		getContentPane().add(txtdataNaixement);
		getContentPane().add(lblpoblacio);
		getContentPane().add(txtpoblacio);
		getContentPane().add(lblopos);
		getContentPane().add(rdbNo);
		getContentPane().add(rdbSi);

		getContentPane().add(lblCP);
		getContentPane().add(txtCP);

		getContentPane().add(lblDni);
		getContentPane().add(txtDni);

		getContentPane().add(pnlbuttons);
		getContentPane().add(lblcombo);
		getContentPane().add(comboprofessors);

	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(5, 5 ,5, 5);

		// lblDni
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblcombo, gbc);

		// txtDNi
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(comboprofessors, gbc);

		// lblDni
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDni, gbc);

		// txtDNi
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtDni, gbc);

		// lblid
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblid, gbc);

		// txtid
		gbc.gridx = 1;
		gbc.gridy = 2;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtid, gbc);

		// lbl nom usuari
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblnomUsuauri, gbc);

		// txt Nom Usuari
		gbc.gridx = 1;
		gbc.gridy = 3;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtnomUsuari, gbc);

		// lbl Nom
		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblnom, gbc);
		// txt Nom
		gbc.gridx = 1;
		gbc.gridy = 4;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtnom, gbc);

		// lbl cognoms
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblcognoms, gbc);
		// txt cognoms
		gbc.gridx = 1;
		gbc.gridy = 5;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtcognoms, gbc);

		// lbl data naixement
		gbc.gridx = 0;
		gbc.gridy = 6;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lbldataNaixement, gbc);

		// txt data naixement
		gbc.gridx = 1;
		gbc.gridy = 6;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtdataNaixement, gbc);

		// lbl poblacio
		gbc.gridx = 0;
		gbc.gridy = 7;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblpoblacio, gbc);

		// txt poblacio
		gbc.gridx = 1;
		gbc.gridy = 7;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtpoblacio, gbc);

		// lbl opos
		gbc.gridx = 0;
		gbc.gridy = 8;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblopos, gbc);

		// rdb Si
		gbc.gridx = 1;
		gbc.gridy = 8;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(rdbSi, gbc);
		// rdb Si
		gbc.gridx = 1;
		gbc.gridy = 9;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(rdbNo, gbc);

		// lbl cp
		gbc.gridx = 0;
		gbc.gridy = 10;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblCP, gbc);

		// txt cp
		gbc.gridx = 1;
		gbc.gridy = 10;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtCP, gbc);

		// pnlButons
		gbc.gridx = 1;
		gbc.gridy = 11;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 0;
		gbc.weighty = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		layout.setConstraints(pnlbuttons, gbc);

	}

	private class Controlador implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if (obj instanceof JButton) {
				JButton boto = (JButton) obj;
				if (boto.getActionCommand().equals("Acceptar")) {
					System.out.println("Acceptar..");
					buidaComponents();
					activarComponents(false);
					comboprofessors.setSelectedIndex(0);
					btnEsborrar.setEnabled(false);

				} else if (boto.getActionCommand().equals("Sortir")) {
					System.out.println("click Sortir.");
					// codi sortir
					ControladorPresentacio.canviFormulari(new FormulariPerDefecte());
					buidaComponents();
				} else if (boto.getActionCommand().equals("Cancelar")) {
					System.out.println("Cancelar");
					// codi CAncelar
					buidaComponents();
					ControladorPresentacio.canviFormulari(new FormulariPerDefecte());
				}else if (boto.getActionCommand().equals("Borrar")){
					Object[] objOpcions3= {"Acceptar","Cancel·lar"};
					int n= JOptionPane.showInternalOptionDialog(Formulariconsultaesborra.getContentPane(), //Finestra pare
					"Segur que Vols donar-lo de baixa?\n amb aquest Accio esborraras tambe els alumnes de aquest profe ", //Missatge de la finestra
					"Baixa usuari", //Títol de la finestra
					JOptionPane.NO_OPTION, //Posarem sempre això.
					JOptionPane.QUESTION_MESSAGE, //Tipus icona
					null, //Si no customitzem la icona. Sinò un ImageIcon
					objOpcions3, //Vector de tipus Object[]
					objOpcions3[0]);//Element per defecte del vector Object[]
					//control del retorn del diàleg (0-primer botó, 1-segon botó, etc)
					if(n==0) {
						System.out.println("Opció triada: Acceptar");
						Object[] objOpcions= {"Acceptar"};
						try {
							ControladorPresentacio.removeProfe(txtDni.getText());
							try {
								Gmail.enviarCorreu("JAVA APLICATION", "accio d'esborrar al teu aplicacio per el professor "+txtDni.getText()+"");
							} catch (GestorDBExcception e1) {
								
								JOptionPane.showInternalOptionDialog(Formulariconsultaesborra.getContentPane(), //Finestra pare
								"ERROR GMAIL: \n"+e1.getMessage(), //Missatge de la finestra
								"Baixa usuari", //Títol de la finestra
								JOptionPane.NO_OPTION, //Posarem sempre això.
								JOptionPane.WARNING_MESSAGE, //Tipus icona
								null, //Si no customitzem la icona. Sinò un ImageIcon
								objOpcions, //Vector de tipus Object[]
								objOpcions[0]);
							}
							comboprofessors.removeItem(comboprofessors.getSelectedItem());
							
							buidaComponents();
							activarComponents(false);
							btnConsultanova.setEnabled(false);
							btnEsborrar.setEnabled(false);
							comboprofessors.setSelectedIndex(0);
						} catch (GestorDBExcception e1) {
							JOptionPane.showInternalOptionDialog(Formulariconsultaesborra.getContentPane(), //Finestra pare
									"ERROR SQL: \n"+e1.getMessage(), //Missatge de la finestra
									" PROFESSOR", //Títol de la finestra
									JOptionPane.NO_OPTION, //Posarem sempre això.
									JOptionPane.WARNING_MESSAGE, //Tipus icona
									null, //Si no customitzem la icona. Sinò un ImageIcon
									objOpcions, //Vector de tipus Object[]
									objOpcions[0]);
						}
						
						
						
					}
					
				}
			}
			if (obj instanceof JComboBox) {
				if (!comboprofessors.getSelectedItem().equals("--Selecciona Dni--")) {
					System.out.println("combo");
					// codi Combo
					comboprofessors.setEnabled(false);
					Professor profe;
					try {
						profe = ControladorPresentacio.getProfe(String.valueOf(comboprofessors.getSelectedItem()));
						if (profe != null) {
							txtDni.setText(profe.getDni());
							txtid.setText(profe.getId());
							if (profe.getTeoposicions()) {
								rdbSi.setSelected(true);
							} else {
								rdbNo.setSelected(true);
							}
							txtCP.setText(String.valueOf(profe.getCP()));
							txtpoblacio.setText(profe.getPoblacio());
							txtcognoms.setText(profe.getCognoms());
							txtnomUsuari.setText(profe.getNomUsuari());
							txtnom.setText(profe.getNom());
							txtdataNaixement.setText(profe.getDataNaixement());
							btnConsultanova.setEnabled(true);
							btnEsborrar.setEnabled(true);
						}
					} catch (GestorDBExcception e1) {
						Object[] objOpcions= {"Acceptar"};
						JOptionPane.showInternalOptionDialog(Formulariconsultaesborra.getContentPane(), //Finestra pare
								"ERROR SQL: \n"+e1.getMessage(), //Missatge de la finestra
								" PROFESSOR", //Títol de la finestra
								JOptionPane.NO_OPTION, //Posarem sempre això.
								JOptionPane.WARNING_MESSAGE, //Tipus icona
								null, //Si no customitzem la icona. Sinò un ImageIcon
								objOpcions, //Vector de tipus Object[]
								objOpcions[0]);
					}
					

				}

			}

		}
		private void activarComponents(Boolean activar) {
			
			if(activar) {
				txtDni.setEnabled(false);
			}else txtDni.setEditable(true);
			
			txtnom.setEnabled(activar);
			txtcognoms.setEnabled(activar);
			
			txtid.setEnabled(activar);
			rdbSi.setEnabled(activar);
			rdbNo.setEnabled(activar);
			txtCP.setEnabled(activar);
			btnConsultanova.setEnabled(activar);
			txtpoblacio.setEnabled(activar);
			txtnomUsuari.setEnabled(activar);
			txtdataNaixement.setEnabled(activar);
			comboprofessors.setEnabled(!activar);
			
		
			
		}
			
			
		}

		private void buidaComponents() {
			txtDni.setText("");
			comboprofessors.grabFocus();
			txtnom.setText("");
			txtcognoms.setText("");
			txtCP.setText("");
			txtdataNaixement.setText("");
			grpButtons.clearSelection();
			txtnomUsuari.setText("");
			txtid.setText("");
			txtpoblacio.setText("");
			
		}
}




