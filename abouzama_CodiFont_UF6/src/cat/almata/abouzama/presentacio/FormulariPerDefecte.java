package cat.almata.abouzama.presentacio;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;

import cat.almata.abouzama.utils.Util;



public class FormulariPerDefecte extends JInternalFrame implements Formulari {
	private JLabel imageFonts;
	private GridBagLayout layout;
	private static final long serialVersionUID = 1L;

	public FormulariPerDefecte() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
		setVisible(true);
	}

	@Override
	public void inicialitzacions() {
		getContentPane().setLayout(layout = new GridBagLayout());
	}

	@Override
	public void crearComponents() {
		imageFonts = new JLabel(Util.redimensionarImatge("imatges/usua.jpg", Aplicacio.WIDTH-100, Aplicacio.HEIGHT-100));
		Util.treureBarraTitolInteralFrame(this);
	}

	@Override
	public void afegirComponents() {
		getContentPane().add(imageFonts);
	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();
		// Posiciona image fonts
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridheight = 1;
		gbc.gridwidth = 1;
		gbc.weightx = 1;
		gbc.weighty = 1;
		//gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.BOTH;
		layout.setConstraints(imageFonts, gbc);

	}
}
