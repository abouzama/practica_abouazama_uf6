package cat.almata.abouzama.controladorAplicacio;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


import cat.almata.abouzama.Excepcions.GestorDBExcception;

public class Gmail {

	// iptn guso wpga sxta
	public static void enviarCorreu(String assumpte, String cos) throws GestorDBExcception {
	    //La dirección de correo de envío
	    String remitente = "bouazamaaissam@gmail.com";
	    //La clave de aplicación obtenida según se explica en este artículo:
	    String claveemailgenerada = "iptngusowpgasxta";
	    
	   String destinatario = "abouzama@almata.cat"; // A quien le quieres escribir.

	   Properties props = new Properties();
		props.setProperty("mail.smtp.host", "smtp.gmail.com");	
		props.setProperty("mail.smtp.user", remitente);
		props.setProperty("mail.smtp.clave", claveemailgenerada );
		props.setProperty("mail.smtp.auth", "true");
		props.setProperty("mail.smtp.starttls.enable", "true");	
		props.setProperty("mail.smtp.port", "587");
	    

	    try {
	    	Session session = Session.getDefaultInstance(props);
	    	// Constrium el message 
		    MimeMessage message = new MimeMessage(session);
		    // aquest es posa el que manda el correo
	        message.setFrom(new InternetAddress(remitente));
	        // aquest es el que recibe el mensage  
	        message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));   //Se podrían añadir varios de la misma manera
	      //aquest es asunto del correo
	        message.setSubject(assumpte);
	        //aquest es el contingut del correo 
	        message.setText(cos);
	        //aquest pas es per enviar el correo
	        Transport transport = session.getTransport("smtp");
	        transport.connect("smtp.gmail.com", remitente, claveemailgenerada);
	        //en aquest puc mandar el correo a moltes persones 
	        transport.sendMessage(message, message.getAllRecipients());
	        //tancar
	        transport.close();
	    }
	    catch (MessagingException me) {
	    	 throw new GestorDBExcception(me.getMessage());
	    }
	  }
}
