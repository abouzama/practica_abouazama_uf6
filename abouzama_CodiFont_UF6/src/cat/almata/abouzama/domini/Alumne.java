package cat.almata.abouzama.domini;

import java.util.Objects;

public class Alumne {
	private String id;
	private String dni;
	private String dateNaixement;
	private String fk_dni_Profe;
	private String nomUsuari;
	private String descripcio;
	private double preuMatricula;
	private boolean Esqui;
	 
	private boolean Natacio;
	private boolean Escalada;
	private boolean Equitacio;
	private boolean Senderisme;
	
	
	
	
	
	
	
	
	public Alumne(String dni) {
		this.dni = dni;
	}
	public Alumne() {
		
	}
	public Alumne(String id, String dni, String nomUsuari, String descripcio, double preuMatricula, boolean esqui,
			boolean natacio, boolean escalada, boolean equitacio, boolean senderisme,String dniProfe,String datenaixement) {
		
		this.id = id;
		this.dni = dni;
		this.nomUsuari = nomUsuari;
		this.descripcio = descripcio;
		this.preuMatricula = preuMatricula;
		this.fk_dni_Profe = dniProfe;
		this.dateNaixement =datenaixement;
		this.Esqui = esqui;
		this.Natacio = natacio;
		this.Escalada = escalada;
		this.Equitacio = equitacio;
		this.Senderisme = senderisme;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNomUsuari() {
		return nomUsuari;
	}
	public void setNomUsuari(String nomUsuari) {
		this.nomUsuari = nomUsuari;
	}
	public String getDescripcio() {
		return descripcio;
	}
	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}
	public double getPreuMatricula() {
		return preuMatricula;
	}
	public void setPreuMatricula(double preuMatricula) {
		this.preuMatricula = preuMatricula;
	}
	public boolean getEsqui() {
		return Esqui;
	}
	public void setEsqui(boolean esqui) {
		Esqui = esqui;
	}
	public boolean getNatacio() {
		return Natacio;
	}
	public void setNatacio(Boolean natacio) {
		Natacio = natacio;
	}
	public boolean getEscalada() {
		return Escalada;
	}
	public void setEscalada(Boolean escalada) {
		Escalada = escalada;
	}
	public boolean getEquitacio() {
		return Equitacio;
	}
	public void setEquitacio(Boolean equitacio) {
		Equitacio = equitacio;
	}
	public boolean getSenderisme() {
		return Senderisme;
	}
	public void setSenderisme(boolean senderisme) {
		Senderisme = senderisme;
	}
	public String getFk_dni_Profe() {
		return fk_dni_Profe;
	}
	public void setFk_dni_Profe(String dniProfe) {
		this.fk_dni_Profe = dniProfe;
	}
	
	@Override
	public String toString() {
		return "Alumne [id=" + id + ", dni=" + dni + ", dniProfe=" + fk_dni_Profe + ", nomUsuari=" + nomUsuari
				+ ", descripcio=" + descripcio + ", preuMatricula=" + preuMatricula + ", Esqui=" + Esqui + ", Natacio="
				+ Natacio + ", Escalada=" + Escalada + ", Equitacio=" + Equitacio + ", Senderisme=" + Senderisme + "]";
	}
	public String getDateNaixement() {
		return dateNaixement;
	}
	public void setDateNaixement(String dateNaixement) {
		this.dateNaixement = dateNaixement;
	}
	@Override
	public int hashCode() {
		return Objects.hash(dni);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alumne other = (Alumne) obj;
		return Objects.equals(dni, other.dni);
	}
	
	
	
	
	
	
}
