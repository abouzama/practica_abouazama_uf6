package cat.almata.abouzama.domini;


import java.util.Objects;

public class Professor {
	private String id;
	private String Dni;
	private String nomUsuari;
	private String nom;
	private String cognoms;
	private String dataNaixement;
	private String poblacio;
	private int CP;
	private boolean teoposicions;	
	
	
	
	// Constructors
	public Professor() {
		
	}
	public Professor(String Dni) {
		this.Dni = Dni;
	}
	public Professor(String Dni,String id, String nomUsuari, String nom, String cognoms, String dataNaixement, String poblacio,
			int cP, Boolean teoposicions) {
		this.Dni = Dni;
		this.id = id;
		this.nomUsuari = nomUsuari;
		this.nom = nom;
		this.cognoms = cognoms;
		this.dataNaixement = dataNaixement;
		this.poblacio = poblacio;
		CP = cP;
		this.teoposicions = teoposicions;
	}
	
	
	//getters and setters 
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNomUsuari() {
		return nomUsuari;
	}
	public void setNomUsuari(String nomUsuari) {
		this.nomUsuari = nomUsuari;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCognoms() {
		return cognoms;
	}
	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}
	public String getDataNaixement() {
		return dataNaixement;
	}
	public void setDataNaixement(String dataNaixement) {
		this.dataNaixement = dataNaixement;
	}
	public String getPoblacio() {
		return poblacio;
	}
	public void setPoblacio(String poblacio) {
		this.poblacio = poblacio;
	}
	public int getCP() {
		return CP;
	}
	public void setCP(int cP) {
		CP = cP;
	}
	public boolean getTeoposicions() {
		return teoposicions;
	}
	
	public void setTeoposicions(Boolean teoposicions) {
		this.teoposicions = teoposicions;
	}
	public String getDni() {
		return Dni;
	}
	public void setDni(String dni) {
		Dni = dni;
	}
	@Override
	public int hashCode() {
		return Objects.hash(Dni);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Professor other = (Professor) obj;
		return Objects.equals(Dni, other.Dni);
	}
	@Override
	public String toString() {
		return "Professor [id=" + id + ", Dni=" + Dni + ", nomUsuari=" + nomUsuari + ", nom=" + nom + ", cognoms="
				+ cognoms + ", dataNaixement=" + dataNaixement + ", poblacio=" + poblacio + ", CP=" + CP
				+ ", teoposicions=" + teoposicions + "]";
	}
	
	
	
	


}
