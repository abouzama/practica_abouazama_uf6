-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql:3306
-- Temps de generació: 22-05-2024 a les 16:39:12
-- Versió del servidor: 5.7.44
-- Versió de PHP: 8.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `examen`
--
CREATE DATABASE IF NOT EXISTS `examen` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `examen`;

-- --------------------------------------------------------

--
-- Estructura de la taula `alumnes`
--

CREATE TABLE `alumnes` (
  `Dni` varchar(9) NOT NULL,
  `id` varchar(30) DEFAULT NULL,
  `dateNaixement` varchar(45) DEFAULT NULL,
  `fk_dni_profe` varchar(9) DEFAULT NULL,
  `nomUsuari` varchar(45) DEFAULT NULL,
  `descripcio` varchar(255) DEFAULT NULL,
  `preuMatricula` double DEFAULT NULL,
  `Esqui` bit(1) DEFAULT NULL,
  `Natacio` bit(1) DEFAULT NULL,
  `Escalada` bit(1) DEFAULT NULL,
  `Equitacio` bit(1) DEFAULT NULL,
  `Senderisme` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `alumnes`
--

INSERT INTO `alumnes` (`Dni`, `id`, `dateNaixement`, `fk_dni_profe`, `nomUsuari`, `descripcio`, `preuMatricula`, `Esqui`, `Natacio`, `Escalada`, `Equitacio`, `Senderisme`) VALUES
('00000I', 'Ais', '15 de maig de 2024', 'Y9', 'abouzama', 'asasasadsaf fgdsgs', 120, b'0', b'1', b'0', b'1', b'1');

-- --------------------------------------------------------

--
-- Estructura de la taula `professors`
--

CREATE TABLE `professors` (
  `Dni` varchar(9) NOT NULL,
  `id` varchar(30) DEFAULT NULL,
  `nomUsuari` varchar(45) DEFAULT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `cognoms` varchar(45) DEFAULT NULL,
  `dataNaixement` varchar(45) DEFAULT NULL,
  `poblacio` varchar(45) DEFAULT NULL,
  `CP` int(11) DEFAULT NULL,
  `teoposicions` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcament de dades per a la taula `professors`
--

INSERT INTO `professors` (`Dni`, `id`, `nomUsuari`, `nom`, `cognoms`, `dataNaixement`, `poblacio`, `CP`, `teoposicions`) VALUES
('Y9', '1232456', '45', '45', '45', '10 de maig de 2024', '45', 0, b'1');

--
-- Índexs per a les taules bolcades
--

--
-- Índexs per a la taula `alumnes`
--
ALTER TABLE `alumnes`
  ADD PRIMARY KEY (`Dni`),
  ADD KEY `fk_dni_profe_idx` (`fk_dni_profe`);

--
-- Índexs per a la taula `professors`
--
ALTER TABLE `professors`
  ADD PRIMARY KEY (`Dni`);

--
-- Restriccions per a les taules bolcades
--

--
-- Restriccions per a la taula `alumnes`
--
ALTER TABLE `alumnes`
  ADD CONSTRAINT `fk_dni_profe` FOREIGN KEY (`fk_dni_profe`) REFERENCES `professors` (`Dni`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
