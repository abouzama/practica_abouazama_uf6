# abouazama_UF6_PRACTICA

##  PRESENTACIO
En el meu projecte la meva aplicació el qui fa és CRUD d'alumnes i de professors i també fa una opció lliure de Gmail (quan es fa una alta o una baixa del professor envia un correu al meu correu electrònic que té el contingut de l'acció feta a la meva aplicació)
### Primera part del professor
hi ha quatre accions per el professor i amb el alumne   

1-la primera accio es l'alta 
2- la segona accio es la Baixa
3- la tercera accio es la consulta 
4- la quarta accio es la modificacio
5- ultima accio es la opcio de consulta Tots (es vol dir qui podem consultar tots els alumnes del professor)
    
![image2](images/classes-projecte.png)  
## I-PROFESSOR
### 1- FORMULARI PRINCIPAL 
com mostra el segünt image aquest es la primera pagina qui surt quan executem el nostre projecte 
![image2](images/ppal.png)   

### 2- FORMULARI ALTA PROFESSOR 
la primera qui hem de fer es accedir als opcions del menu del Gestio professor i a la opcio de "ALTA PROFESSOR"   
![image2](images/fapp.png) 

    

***la primera cosa que tenim que fer es omplir els camps i per fer una ALTA Correcta tenim que omplir el Camp Clau***    
**Exemple**
per fer una alta correcta hi ha de picar al boto Acceptar
![image2](images/fop.png) 
     
          

<B>en el cas Contrari de no Omplir el camp del DNI surt una excepcio</B>    
     
           

![image2](images/fep.png) 
     

**hi ha moltes excepcions Controlats pero anem del sql que poden passar**    

**la primera cosa que pod passar la contraseña del PHPMyAdmin no es correcta**    
      
          


![image2](images/eSQLe.png) 

### 2- FORMULARI CONSULTA ESBORRA PROFESSOR 
Aquesta Formulari tè al principi un combo Box dels Professors que volem consultar per a qui fem una consulta Correcta hem de seleccionar el professor que volem Consultar te tres botons una del Consulta Nova lo qui fa es (preparar el Formulari per un Consulta Nova d'un altre profe)
i el Boton Borrar esborar el professor i el seus Alumnes
![image2](images/FCPP.png) 

**en el Cas de picar el Buton esborrar surt un internal panel que es digui que esborrar el professor esborras tambe els alumnes**
![image2](images/FBPP.png) 

### 3- FORMULARI CONSULTA ALUMNES DE PROFESSOR
En aquesta Formulari tenim un combo de professors per els que volem Consultar els Alumnes (i per Consultar els alumnes hem de seleccionar professor)

![image2](images/FCONSTOTS.png) 

### 4- FORMULARI MODIFICACIÓ DE PROFESSOR
el formulari Modificacio te un Boton de cercar i quan seleccionarem la primera vegada al el componenets estan desactivados i per Activar els Camps tenim que cercar el nostre professor que volem modificar en el cas de donar un DNI no existeix dona surt una excepcion de Aquest profe no existeix hi ha altres excepcions que estan controlats




![image2](images/FMP.png)       


**en el cas del professor no existeix**     
   Exemple

![image2](images/emodifica.png)  
 
     
**en el cas del professor existeix**
    Exemple
![image2](images/fmodificac.png)

## II-ALUMNE
### I-1- FORMULARI ALTA ALUMNES
***Aquesta Formulari te un combo de professors que estan donats d'alta a la meva BBDD***
    
Aquesta es l'imatge principal del meu Formulari Alta 
     

![image2](images/FAAA.png)    

com mostrala imatge els camps estan desactivats per activar els camps hi ha de Seleccionar Algun Professor


![image2](images/fmalumne.png) 

     
### I-1- FORMULARI CONSULTA ESBORRA ALUMNE

Aquest Formulari té un combo d'alumnes qui esta en Alta en la meva BBDD i els camps no es poden modificar per que es una Consulta esborrar    

![image2](images/fcalumne.png)     

 
el Formulari té el Boton Borrar por esborrar el Alumne 
quan pices surt Internal Panel si volem confirmar qui estem segu al esborrar aquest Alumne   

![image2](images/fbc.png)  

### I-1- FORMULARI MODIFICACIÓ ALUMNE

**EN el cas de la modificacio té un combo que te els Alumnes qui estan donats d'alta i tenim qui seleccionar el Alumne que volem modificar**

![image2](images/MODIFICACIO.png) 
